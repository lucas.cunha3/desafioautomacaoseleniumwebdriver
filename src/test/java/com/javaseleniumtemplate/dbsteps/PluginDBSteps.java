package com.javaseleniumtemplate.dbsteps;

import com.javaseleniumtemplate.utils.DBUtils;
import com.javaseleniumtemplate.utils.Utils;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class PluginDBSteps {


    private static String queriesPath = "src/test/java/com/javaseleniumtemplate/queries/pluginqueries/";

    public static ArrayList<String> retornaPluginDB() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String query = Utils.getFileContent(queriesPath + "retornaPluginInstalados.sql");
        return DBUtils.getQueryResult(query);

    }

}



