package com.javaseleniumtemplate.dbsteps;


import com.javaseleniumtemplate.utils.DBUtils;
import com.javaseleniumtemplate.utils.Utils;
import java.io.*;

public class RestoreDBSteps {

    private static String restorePath = "src/test/java/com/javaseleniumtemplate/queries/restorequeries/";

    public static void executaRestoreBD () throws FileNotFoundException {
        Reader reader = Utils.getFileContentInReader(restorePath + "restoreTest.sql");
        DBUtils.updateDatabase(reader);

    }

}

