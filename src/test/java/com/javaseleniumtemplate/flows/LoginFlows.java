package com.javaseleniumtemplate.flows;

import com.javaseleniumtemplate.pages.LoginPage;
import com.javaseleniumtemplate.pages.LoginPasswordPage;

public class LoginFlows {
    //Objects and constructor
    LoginPage loginPage;
    LoginPasswordPage loginPasswordPage;

    public LoginFlows(){
        loginPage = new LoginPage();
        loginPasswordPage = new LoginPasswordPage();
    }

    //Flows
    public void efetuarLogin(String username, String password){
        loginPage.preencherUsuario(username);
        loginPage.clicarEmEntrar();
        loginPasswordPage.preencherSenha(password);
        loginPasswordPage.clicarEmEntrar();
    }
}
