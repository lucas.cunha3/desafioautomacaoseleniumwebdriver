package com.javaseleniumtemplate.flows;


import com.javaseleniumtemplate.pages.MainPage;
import com.javaseleniumtemplate.pages.ManageOverviewPage;

public class ManageFlows {

    //Objects and constructor
    MainPage mainPage;
    ManageOverviewPage manageOverviewPage;

    public ManageFlows(){
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
    }

    //Flows
    public void visualizarPaginaProjetos(){
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarProjetos();
    }


}
