package com.javaseleniumtemplate.flows;

import com.javaseleniumtemplate.pages.CreateTaskPage;

public class TaskFlows {

    //Objects and constructor
    CreateTaskPage createTaskPage;

    public TaskFlows(){
        createTaskPage = new CreateTaskPage();
    }

    //Flows
    public void criarTarefa(String categoria, String frequencia, String gravidade, String prioridade, String resumo, String descricao){
        createTaskPage.selecionarCategoria(categoria);
        createTaskPage.selecionarFrequencia(frequencia);
        createTaskPage.selecionarGravidade(gravidade);
        createTaskPage.selecionarPrioridade(prioridade);
        createTaskPage.preencherResumo(resumo);
        createTaskPage.preencherDescricao(descricao);
        createTaskPage.clicarEmCriarNovaTarefa();
    }
}



