package com.javaseleniumtemplate.tests;

import com.javaseleniumtemplate.bases.TestBase;
import com.javaseleniumtemplate.dbsteps.RestoreDBSteps;
import com.javaseleniumtemplate.dbsteps.PluginDBSteps;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


public class RestoreDatabaseTests extends TestBase {

    RestoreDBSteps restoreDBSteps;
    PluginDBSteps pluginDBSteps;
    ManageTests manageTests;

    @Test
    public void verificarAlteracaoNoDB() throws UnsupportedEncodingException, NoSuchAlgorithmException, FileNotFoundException {

        //Objects Instances
        pluginDBSteps = new PluginDBSteps();
        manageTests = new ManageTests();
        restoreDBSteps = new RestoreDBSteps();

        //Parameteres
        ArrayList<String> list = null;

        //Test
        list = PluginDBSteps.retornaPluginDB(); //Armazenar nome dos plugins instalados em uma ArrayList
        manageTests.instalarPluginComSucesso_0039(); //Instalar um plugin
        RestoreDBSteps.executaRestoreBD();

        Assert.assertEquals(list, PluginDBSteps.retornaPluginDB());


    }
}



