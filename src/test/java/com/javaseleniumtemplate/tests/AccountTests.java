package com.javaseleniumtemplate.tests;

import com.javaseleniumtemplate.bases.TestBase;
import com.javaseleniumtemplate.flows.LoginFlows;
import com.javaseleniumtemplate.pages.AccountPage;
import com.javaseleniumtemplate.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AccountTests extends TestBase {
    LoginFlows loginFlows;
    MainPage mainPage;
    AccountPage accountPage;

    @Test
    public void criarAPIComSucesso_0011(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        accountPage = new AccountPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeNovaAPI = "Teste API no Template Base2 Final";
        String mensagemEsperada = "Tenha em conta que este token só será exibido uma vez.";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarNoUsuarioLogado();
        mainPage.clicarEmMinhaConta();
        accountPage.clicarEmTokensAPI();
        accountPage.inserirNomeAPI(nomeNovaAPI);
        accountPage.clicarEmCriarAPI();

        Assert.assertEquals(mensagemEsperada, accountPage.retornarMensagemDeTokenGerado());

    }

    @Test
    public void criarAPIComNomeJaExistente_0012(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        accountPage = new AccountPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeNovaAPI = "Teste API no Template Base2";
        String mensagemErroEsperada = "APPLICATION ERROR #3000\n" +
                "O nome do Token API \"" + nomeNovaAPI + "\" já esta em uso. Por favor, volte e selecione outro.\n" +
                "Por favor, utilize o botão \"Voltar\" de seu navegador web para voltar à pagina anterior. Lá você pode corrigir quaisquer problemas identificados neste erro ou escolher uma outra ação. Você também pode clicar em uma opção da barra de menus para ir diretamente para outra seção.";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarNoUsuarioLogado();
        mainPage.clicarEmMinhaConta();
        accountPage.clicarEmTokensAPI();
        accountPage.inserirNomeAPI(nomeNovaAPI);
        accountPage.clicarEmCriarAPI();

        Assert.assertEquals(mensagemErroEsperada, accountPage.retornaMensagemDeErroToken());

    }

    @Test
    public void criarAPISemNome_0013(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        accountPage = new AccountPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeNovaAPI = "";
        String mensagemErroEsperada = "APPLICATION ERROR #11\n" +
                "Um campo necessário 'Nome do Token' estava vazio. Por favor, verifique novamente suas entradas.\n" +
                "Por favor, utilize o botão \"Voltar\" de seu navegador web para voltar à pagina anterior. Lá você pode corrigir quaisquer problemas identificados neste erro ou escolher uma outra ação. Você também pode clicar em uma opção da barra de menus para ir diretamente para outra seção.";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarNoUsuarioLogado();
        mainPage.clicarEmMinhaConta();
        accountPage.clicarEmTokensAPI();
        accountPage.inserirNomeAPI(nomeNovaAPI);
        accountPage.clicarEmCriarAPI();

        Assert.assertEquals(mensagemErroEsperada, accountPage.retornaMensagemDeErroToken());
    }

    @Test
    public void revogarAcessoAPI_0014() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        accountPage = new AccountPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeAPIParaRevogar = "Teste API no Template Base2";
        String mensagemEsperada = "O Token API \"" + nomeAPIParaRevogar + "\" foi revogado.";


        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarNoUsuarioLogado();
        mainPage.clicarEmMinhaConta();
        accountPage.clicarEmTokensAPI();
        accountPage.clicarEmRevogarAcesso(nomeAPIParaRevogar);

        Assert.assertEquals(mensagemEsperada, accountPage.retornarMensagemDeTokenRevogado());
    }

    @Test
    public void alterarFusoHorario_00015(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        accountPage = new AccountPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String fusoHorario = "Sao Paulo";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarNoUsuarioLogado();
        mainPage.clicarEmMinhaConta();
        accountPage.clicarEmPreferencias();
        accountPage.selecionarFusoHorario(fusoHorario);
        accountPage.clicarEmSalvarPreferencias();

        Assert.assertEquals(fusoHorario, accountPage.retornarFusoHorarioSelecionado());
    }

    @Test
    public void alterarTempoDeRenovacao_00016(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        accountPage = new AccountPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String tempoNovoDeRenovacao = "15";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarNoUsuarioLogado();
        mainPage.clicarEmMinhaConta();
        accountPage.clicarEmPreferencias();
        accountPage.alterarTempoDeRenovacao(tempoNovoDeRenovacao);
        accountPage.clicarEmSalvarPreferencias();

        Assert.assertEquals(tempoNovoDeRenovacao, accountPage.retornarTempoDeRenovacaoSalvo());
    }

    @Test
    public void reiniciarConfiguracoesDeColuna_0048() {

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        accountPage = new AccountPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String mensagemDeSucessoEsperada = "Operação realizada com sucesso.";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarNoUsuarioLogado();
        mainPage.clicarEmMinhaConta();
        accountPage.clicarEmGerenciarColunas();
        accountPage.clicarEmReinicializarConfiguracoesDasColunas();

        Assert.assertEquals(mensagemDeSucessoEsperada, accountPage.retornaMensagemSucesso());

    }


}

