package com.javaseleniumtemplate.tests;

import com.javaseleniumtemplate.bases.PageBase;
import com.javaseleniumtemplate.bases.TestBase;
import com.javaseleniumtemplate.flows.LoginFlows;
import com.javaseleniumtemplate.flows.ManageFlows;
import com.javaseleniumtemplate.pages.*;
import com.javaseleniumtemplate.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;

public class ManageTests extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    ManageOverviewPage manageOverviewPage;
    ManageProjectsPage manageProjectsPage;
    ManageFlows manageFlows;
    ManageTagsPage manageTagsPage;
    PageBase pageBase;
    ManageGlobalProfilesPage manageGlobalProfilesPage;
    ManageUsersPage manageUsersPage;
    ManagePluginPage managePluginPage;



    //Tests
    @Test
    public void validarVersaoMantis_0003() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String versaoMantisBT = "2.22.1";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();

        Assert.assertEquals(versaoMantisBT, manageOverviewPage.retornaVersaoMantis());
    }

    @Test
    public void validarVersaoSchema_0004() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String versaoSchema = "209";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();

        Assert.assertEquals(versaoSchema, manageOverviewPage.retornaVersaoSchema());
    }

    @Test
    public void criarProjetoComSucesso_0006(){
        //Objects instances
        loginFlows = new LoginFlows();
        manageProjectsPage = new ManageProjectsPage();
        manageFlows = new ManageFlows();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeProjeto = "Teste Formatado Reteste";
        String statusProjeto = "release";
        String visibilidadeProjeto = "público";
        String descricaoProjeto = "Descrição do Projeto Utilizando Template Base2";
        String mensagemDeSucessoEsperada = "Operação realizada com sucesso.";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        manageFlows.visualizarPaginaProjetos();
        manageProjectsPage.clicarEmCriarProjeto();
        manageProjectsPage.inserirNomeNovoProjeto(nomeProjeto);
        manageProjectsPage.definirStatusNovoProjeto(statusProjeto);
        manageProjectsPage.definirVisibilidadeNovoProjeto(visibilidadeProjeto);
        manageProjectsPage.inserirDescricaoNovoProjeto(descricaoProjeto);
        manageProjectsPage.clicarEmAdicionarProjeto();

        Assert.assertEquals(mensagemDeSucessoEsperada, manageProjectsPage.retornaMensagemSucesso());
    }

    @Test
    public void criarProjetoComErro_0007() {
        //Objects instances
        loginFlows = new LoginFlows();
        manageProjectsPage = new ManageProjectsPage();
        manageFlows = new ManageFlows();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeProjeto = "";
        String statusProjeto = "release";
        String visibilidadeProjeto = "público";
        String descricaoProjeto = "Descrição do Projeto Teste Para Gerar Erro";
        String tituloEsperado = "Adicionar projeto";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        manageFlows.visualizarPaginaProjetos();
        manageProjectsPage.clicarEmCriarProjeto();
        manageProjectsPage.inserirNomeNovoProjeto(nomeProjeto);
        manageProjectsPage.definirStatusNovoProjeto(statusProjeto);
        manageProjectsPage.definirVisibilidadeNovoProjeto(visibilidadeProjeto);
        manageProjectsPage.inserirDescricaoNovoProjeto(descricaoProjeto);
        manageProjectsPage.clicarEmAdicionarProjeto();

        Assert.assertEquals(tituloEsperado, manageProjectsPage.retornaTituloDaTabela());
    }

    @Test
    public void apagarProjetoComSucesso_0008(){
        //Objects instances
        loginFlows = new LoginFlows();
        manageFlows = new ManageFlows();
        manageProjectsPage = new ManageProjectsPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeProjeto = "Teste Formatado Reteste";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        manageFlows.visualizarPaginaProjetos();
        manageProjectsPage.clicarEmProjeto(nomeProjeto);
        manageProjectsPage.apagarProjeto();

        Assert.assertFalse(manageProjectsPage.retornaSeOProjetoExisteNaTabela(nomeProjeto));
    }

    @Test
    public void criarMarcadorComSucesso_0009(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageTagsPage = new ManageTagsPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeMarcador = "Marcador Formatado";
        String descricaoMarcador = "Descrição do Marcador pare execução de Testes no Template Base2";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarMarcadores();
        manageTagsPage.clicarEmCriarMarcador();
        manageTagsPage.preencherNomeMarcador(nomeMarcador);
        manageTagsPage.preencherDescricaoMarcador(descricaoMarcador);
        manageTagsPage.clicarEmCriarNovoMarcador();

        Assert.assertTrue(manageTagsPage.retornaSeMarcadorFoiCriado(nomeMarcador));

    }

    @Test
    public void criarMarcadorComFalha_0010() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageTagsPage = new ManageTagsPage();
        pageBase = new PageBase();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeMarcador = "";
        String descricaoMarcador = "Descrição do Marcador pare execução de Testes no Template Base2";
        String linkCriacaoTag = "http://127.0.0.1:8989/manage_tags_page.php#tagcreate";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarMarcadores();
        manageTagsPage.clicarEmCriarMarcador();
        manageTagsPage.preencherNomeMarcador(nomeMarcador);
        manageTagsPage.preencherDescricaoMarcador(descricaoMarcador);
        manageTagsPage.clicarEmCriarNovoMarcador();

        Assert.assertEquals(linkCriacaoTag, pageBase.getURL());

    }

    @Test
    public void apagarMarcadorComSucesso_0035() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageTagsPage = new ManageTagsPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String marcadorParaDeletar = "Marcador Formatado";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarMarcadores();
        manageTagsPage.clicarEmDeterminadoMarcador(marcadorParaDeletar);
        manageTagsPage.clicarEmApagarMarcador();

        Assert.assertFalse(manageTagsPage.verificarSeMarcadorExiste(marcadorParaDeletar));

    }

    @Test
    public void criarPerfilGlobal_0017() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageTagsPage = new ManageTagsPage();
        pageBase = new PageBase();
        manageGlobalProfilesPage = new ManageGlobalProfilesPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String plataforma = "Windows";
        String os = "Sistema operacional de 64 bits";
        String versaoOS = "20H2";
        String descricao = "Processador: Intel(R) Core(TM) i5-8265U CPU @ 1.60GHz   1.80 GHz\n" +
                "RAM: 8,00 GB";
        String nomePerfilGlobalNaLista = plataforma + " " + os + " " + versaoOS;

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarPerfisGlobais();
        manageGlobalProfilesPage.preencherPlataforma(plataforma);
        manageGlobalProfilesPage.preencherOS(os);
        manageGlobalProfilesPage.preencherVersaoOS(versaoOS);
        manageGlobalProfilesPage.preencherDescricao(descricao);
        manageGlobalProfilesPage.clicarEmAdicionarPerfilGlobal();

        Assert.assertEquals(nomePerfilGlobalNaLista, manageGlobalProfilesPage.retornarNomePerfil(nomePerfilGlobalNaLista));
    }

    @Test
    public void apagarPerfilGlobal_0018(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageTagsPage = new ManageTagsPage();
        pageBase = new PageBase();
        manageGlobalProfilesPage = new ManageGlobalProfilesPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomePerfilGlobalNaLista = "Windows Sistema operacional de 64 bits 20H2";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarPerfisGlobais();
        manageGlobalProfilesPage.selecionarApagarPerfil();
        manageGlobalProfilesPage.selecionarPerfilGlobalNaLista(nomePerfilGlobalNaLista);
        manageGlobalProfilesPage.clicarEmEnviar();

        Assert.assertFalse(manageGlobalProfilesPage.verificarSePerfilExiste(nomePerfilGlobalNaLista));
    }

    @Test
    public void editarPerfilGlobal_0019(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageTagsPage = new ManageTagsPage();
        pageBase = new PageBase();
        manageGlobalProfilesPage = new ManageGlobalProfilesPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomePerfilGlobalNaLista = "Windows Sistema operacional de 64 bits 20H2";
        String plataforma = "Windows_V2";
        String os = "OS de 64 bits_V2";
        String versaoOS = "20H2_V2";
        String descricao = "Processador: Intel(R) Core(TM) i5-8265U CPU @ 1.60GHz   1.80 GHz\n" +
                "RAM: 8,00 GB_V2";
        String nomePerfilGlobalNaListaAtualizado = plataforma + " " + os + " " + versaoOS;

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarPerfisGlobais();
        manageGlobalProfilesPage.selecionarEditarPerfil();
        manageGlobalProfilesPage.selecionarPerfilGlobalNaLista(nomePerfilGlobalNaLista);
        manageGlobalProfilesPage.clicarEmEnviar();
        manageGlobalProfilesPage.preencherPlataforma(plataforma);
        manageGlobalProfilesPage.preencherOS(os);
        manageGlobalProfilesPage.preencherVersaoOS(versaoOS);
        manageGlobalProfilesPage.preencherDescricao(descricao);
        manageGlobalProfilesPage.clicarEmAtualizarPerfilGlobal();

        Assert.assertEquals(nomePerfilGlobalNaListaAtualizado, manageGlobalProfilesPage.retornarNomePerfil(nomePerfilGlobalNaListaAtualizado));

    }

    @Test
    public void criarUsuarioComSucesso_0020(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeDeUsuario = "Matheus.Santos";
        String nomeReal = "Matheus Santos Pereira";
        String emailUsuario ="matheus.santos.pereira@base2.com.br";
        String nivelDeAcesso = "visualizador";
        String mensagemEsperada = "Usuário " + nomeDeUsuario +  " criado com um nível de acesso de " + nivelDeAcesso;

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarUsuarios();
        manageUsersPage.clicarEmCriarNovaConta();
        manageUsersPage.preencherNomeDeUsuario(nomeDeUsuario);
        manageUsersPage.preencherNomeRealDeUsuario(nomeReal);
        manageUsersPage.preencherEmailDeUsuario(emailUsuario);
        manageUsersPage.selecionarNivelDeAcessoDoUsuario(nivelDeAcesso);
        manageUsersPage.clicarEmCriarNovoUsuario();

        Assert.assertEquals(mensagemEsperada,manageUsersPage.retornarMensagemDeSucesso());

    }

    @Test
    public void criarUsuarioSemNome_0021(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeDeUsuario = ""; //nome de usuario vazio
        String nomeReal = "Matheus Santos Pereira";
        String emailUsuario ="matheus.santos.pereira@base2.com.br";
        String nivelDeAcesso = "visualizador";
        String mensagemErroEsperada = "APPLICATION ERROR #805\n" +
                "O nome de usuário não é inválido. Nomes de usuário podem conter apenas letras, números, espaços, hífens, pontos, sinais de mais e sublinhados.\n" +
                "Por favor, utilize o botão \"Voltar\" de seu navegador web para voltar à pagina anterior. Lá você pode corrigir quaisquer problemas identificados neste erro ou escolher uma outra ação. Você também pode clicar em uma opção da barra de menus para ir diretamente para outra seção.";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarUsuarios();
        manageUsersPage.clicarEmCriarNovaConta();
        manageUsersPage.preencherNomeDeUsuario(nomeDeUsuario);
        manageUsersPage.preencherNomeRealDeUsuario(nomeReal);
        manageUsersPage.preencherEmailDeUsuario(emailUsuario);
        manageUsersPage.selecionarNivelDeAcessoDoUsuario(nivelDeAcesso);
        manageUsersPage.clicarEmCriarNovoUsuario();

        Assert.assertEquals(mensagemErroEsperada, manageUsersPage.retornaMensagemDeErro());
    }

    @Test
    public void apagarUsuarioComSucesso_0022(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeDeUsuario = "Matheus.Santos";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarUsuarios();
        manageUsersPage.clicarEmDeterminadoUsuario(nomeDeUsuario);
        manageUsersPage.clicarApagarUsuario();

        Assert.assertFalse(manageUsersPage.verificarSeUsuarioExiste(nomeDeUsuario));

    }

    @Test
    public void criarCategoriaGlobalComSucesso_0023(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();
        manageProjectsPage = new ManageProjectsPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeNovaCategoria =  "Teste Formatado";
        String mensagemDeSucessoEsperada = "Operação realizada com sucesso.";
        String usuarioASerAtribuidoCategoria = "administrator";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarProjetos();
        manageProjectsPage.preencherNomeCategoria(nomeNovaCategoria);
        manageProjectsPage.clicarAdicionarEEditarCategoria();
        manageProjectsPage.atribuirCategoriaAUsuario(usuarioASerAtribuidoCategoria);
        manageProjectsPage.clicarEmAtualizarCategoria();

        Assert.assertEquals(mensagemDeSucessoEsperada, manageProjectsPage.retornaMensagemSucesso());

    }

    @Test
    public void criarCategoriaGlobalComFalha_0024(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();
        manageProjectsPage = new ManageProjectsPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomeNovaCategoria =  "";
        String mensagemErroEsperada = "APPLICATION ERROR #11\n" +
                "Um campo necessário 'Categoria' estava vazio. Por favor, verifique novamente suas entradas.\n" +
                "Por favor, utilize o botão \"Voltar\" de seu navegador web para voltar à pagina anterior. Lá você pode corrigir quaisquer problemas identificados neste erro ou escolher uma outra ação. Você também pode clicar em uma opção da barra de menus para ir diretamente para outra seção.";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarProjetos();
        manageProjectsPage.preencherNomeCategoria(nomeNovaCategoria);
        manageProjectsPage.clicarAdicionarEEditarCategoria();

        Assert.assertEquals(mensagemErroEsperada, manageProjectsPage.retornaMensagemErro());
    }

    @Test
    public void alterarPermissaoDeUsuarioComSucesso_0029(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageUsersPage = new ManageUsersPage();
        manageOverviewPage = new ManageOverviewPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String usuarioParaAlterar = "Lucas.Cunha";
        String escolherNivelDeAcesso = "desenvolvedor";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarUsuarios();
        manageUsersPage.clicarEmDeterminadoUsuario(usuarioParaAlterar);
        manageUsersPage.alterarNivelDeAcessoDoUsuario(escolherNivelDeAcesso);
        manageUsersPage.clicarEmAtualizarUsuario();

        Assert.assertEquals(escolherNivelDeAcesso, manageUsersPage.retornarOpcaoSelecionadaDeNivelDeAcesso());

    }

    @Test
    public void adicionarUsuarioAProjetoPrivado_0030() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageUsersPage = new ManageUsersPage();
        manageOverviewPage = new ManageOverviewPage();
        manageProjectsPage = new ManageProjectsPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String projetoEscolhido = "hello";
        String nomeUsuarioParaAdicionar = "Lucas.Cunha";
        String nivelDeAcesso = "gerente";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarProjetos();
        manageProjectsPage.clicarEmProjeto(projetoEscolhido);
        manageProjectsPage.selecionarUsuarioParaAdicionarAProjetoPrivado(nomeUsuarioParaAdicionar);
        manageProjectsPage.selecionarNivelDeAcessoDoUsuario(nivelDeAcesso);
        manageProjectsPage.adicionarUsuarioAProjetoPrivado();

        Assert.assertTrue(manageProjectsPage.verificarSeUsuarioFoiAdicionadoAProjeto(nomeUsuarioParaAdicionar));

    }

    @Test
    public void removerUsuarioDeProjetoPrivado_0031() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageUsersPage = new ManageUsersPage();
        manageOverviewPage = new ManageOverviewPage();
        manageProjectsPage = new ManageProjectsPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String projetoEscolhido = "hello";
        String usuarioParaRevogar = "Lucas.Cunha";
        String mensagemEsperada = "Operação realizada com sucesso.";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarProjetos();
        manageProjectsPage.clicarEmProjeto(projetoEscolhido);
        manageProjectsPage.clicarEmRevogarAcessoDeDeterminadoUsuario(usuarioParaRevogar);
        manageProjectsPage.clicarEmAplicarMudancas();
        manageProjectsPage.clicarEmConfirmarMudancas();

        Assert.assertEquals(mensagemEsperada, manageProjectsPage.retornaMensagemSucesso());
    }

    @Test
    public void instalarPluginComSucesso_0039(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        managePluginPage = new ManagePluginPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomePlugin = "Gráficos Mantis 2.22.1";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarPlugins();
        managePluginPage.clicarEmInstalarPlugin(nomePlugin);

        Assert.assertEquals(nomePlugin, managePluginPage.retornarNomeDoPlugin(nomePlugin));

    }

    @Test
    public void desinstalarPluginComSucesso_0040(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        managePluginPage = new ManagePluginPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nomePlugin = "Gráficos Mantis 2.22.1";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarPlugins();
        managePluginPage.clicarEmDesinstalarPlugin(nomePlugin);
        managePluginPage.clicarNoBotaoDesinstalar();

        Assert.assertFalse(managePluginPage.retornarSePluginEstaInstalado(nomePlugin));

    }

    @Test
    public void realizarFiltroClicandoEmLetra_0041() {

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String letraSelecionado = "T";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarUsuarios();
        manageUsersPage.clicarEmUmaLetra(letraSelecionado);

        Assert.assertEquals(letraSelecionado, manageUsersPage.retornarLetraSelecionada());

    }

    @DataProvider(name = "dataProjetoCSVProvider")
    public Iterator<Object[]> dataProjetoProvider() {
        return  Utils.csvProvider("src/test/java/dataProjeto.csv");
    }

    @Test(dataProvider = "dataProjetoCSVProvider")
    public void criarProjetoUtilizadoDataDriven_0050(String[] projetoData) {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageProjectsPage = new ManageProjectsPage();


        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String nome = projetoData[0];
        String estado = projetoData[1];
        String descricao = projetoData[2];
        String mensagemDeSucessoEsperada = "Operação realizada com sucesso.";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarProjetos();
        manageProjectsPage.clicarEmCriarProjeto();

        manageProjectsPage.inserirNomeNovoProjeto(nome);
        manageProjectsPage.definirStatusNovoProjeto(estado);
        manageProjectsPage.inserirDescricaoNovoProjeto(descricao);
        manageProjectsPage.clicarEmAdicionarProjeto();

        Assert.assertEquals(mensagemDeSucessoEsperada, manageProjectsPage.retornaMensagemSucesso());



    }

    @Test
    public void desabilitarUsuario_0051(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String usuarioHabilitado = "Lucas.Cunha";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarUsuarios();
        manageUsersPage.clicarEmDeterminadoUsuario(usuarioHabilitado);
        manageUsersPage.clicarNoCheckBoxHabilitar();
        manageUsersPage.clicarEmAtualizarPreferencias();

        Assert.assertNull(manageUsersPage.retornarSeBotaoHabilitarEstaMarcado());

    }

    @Test
    public void habilitarUsuario_0052() {

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String usuarioParaHabilitar = "Lucas.Cunha";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarUsuarios();
        manageUsersPage.marcarEmMostrarDesativados();
        manageUsersPage.clicarEmAplicarFiltro();
        manageUsersPage.clicarEmDeterminadoUsuario(usuarioParaHabilitar);
        manageUsersPage.clicarNoCheckBoxHabilitar();
        manageUsersPage.clicarEmAtualizarPreferencias();

        Assert.assertEquals("true", manageUsersPage.retornarSeBotaoHabilitarEstaMarcado());

    }

}
