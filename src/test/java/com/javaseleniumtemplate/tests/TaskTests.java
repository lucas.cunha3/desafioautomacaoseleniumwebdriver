package com.javaseleniumtemplate.tests;

import com.javaseleniumtemplate.bases.PageBase;
import com.javaseleniumtemplate.bases.TestBase;
import com.javaseleniumtemplate.flows.LoginFlows;
import com.javaseleniumtemplate.flows.TaskFlows;
import com.javaseleniumtemplate.pages.CreateTaskPage;
import com.javaseleniumtemplate.pages.MainPage;
import com.javaseleniumtemplate.pages.MyViewPage;
import com.javaseleniumtemplate.pages.TaskPage;
import com.javaseleniumtemplate.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.reporters.jq.Main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class TaskTests extends TestBase {
    LoginFlows loginFlows;
    MainPage mainPage;
    CreateTaskPage createTaskPage;
    TaskPage taskPage;
    MyViewPage myViewPage;
    PageBase pageBase;
    TaskFlows taskFlows;


    @Test
    public void criarTarefaComSucesso_0032(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        createTaskPage = new CreateTaskPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String projeto = "hello";
        String categoria = "[Todos os Projetos] General";
        String frequencia = "sempre";
        String gravidade = "travamento";
        String prioridade = "imediato";
        String resumo = "Teste Resumo3";
        String descricao = "Teste Descrição3";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmCriarTarefa();
        createTaskPage.selecionarProjeto(projeto);
        createTaskPage.clicarEmSelecionarProjeto();
        createTaskPage.selecionarCategoria(categoria);
        createTaskPage.selecionarFrequencia(frequencia);
        createTaskPage.selecionarGravidade(gravidade);
        createTaskPage.selecionarPrioridade(prioridade);
        createTaskPage.preencherResumo(resumo);
        createTaskPage.preencherDescricao(descricao);
        createTaskPage.clicarEmCriarNovaTarefa();

        Assert.assertEquals(categoria,  createTaskPage.retornarCategoriaPreenchida());
        Assert.assertEquals(frequencia, createTaskPage.retornarFrequenciaPreenchida());
        Assert.assertEquals(gravidade, createTaskPage.retornarGravidadePreenchida());
        Assert.assertEquals(prioridade, createTaskPage.retornarPrioridadePreenchida());
        Assert.assertEquals(descricao, createTaskPage.retornarDescricaoPreenchida());
        Assert.assertTrue(createTaskPage.retornarResumoPreenchida().endsWith(resumo));

    }

    @Test
    public void apagarTarefaComSucesso_0033(){

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        taskPage = new TaskPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String numeroTarefa = "0000042";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmTarefas();
        taskPage.clicarEmDeterminadaTarega(numeroTarefa);
        taskPage.clicarEmDeletarTarefa();
        taskPage.clicarEmDeletar();

        Assert.assertFalse(taskPage.retornaSeTarefaExiste(numeroTarefa));

    }

    @Test
    public void atribuirTarefaAUsuario_0034() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        myViewPage = new MyViewPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String numeroProjeto = "0000039";
        String usuarioAtribuido = "Lucas.Cunha";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmMinhaVisao();
        myViewPage.clicarEmDeterminadoProjetoNaoAtribuido(numeroProjeto);
        myViewPage.clicarEmAtualizar();
        myViewPage.selecionarUsuarioParaAtribuirTarefa(usuarioAtribuido);
        myViewPage.clicarEmAtualizarInformacoes();

        Assert.assertEquals("Lucas.Cunha", myViewPage.retornarNomeUsuarioAtribuido());

    }

    @Test
    public void verificarRecentementeVisitados_0036() {

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        taskPage = new TaskPage();
        pageBase = new PageBase();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String numeroTarefa = "0000015";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmTarefas();
        taskPage.clicarEmDeterminadaTarega(numeroTarefa);
        pageBase.refresh();

        Assert.assertEquals(numeroTarefa, taskPage.retornaUltimaTarefaVisitada());
    }

    @Test
    public void realizarPesquisaDeUmaTarefaQueNaoExiste_0042(){
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        taskPage = new TaskPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String numeroDaTarefa = "0000001";
        int numeroDaTarefaInt = Integer.parseInt(numeroDaTarefa);
        String mensagemErroEsperada = "APPLICATION ERROR #1100\n" +
                "A tarefa " + numeroDaTarefaInt + " não encontrada.\n" +
                "Por favor, utilize o botão \"Voltar\" de seu navegador web para voltar à pagina anterior. Lá você pode corrigir quaisquer problemas identificados neste erro ou escolher uma outra ação. Você também pode clicar em uma opção da barra de menus para ir diretamente para outra seção.";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmTarefas();
        taskPage.preencherERealizarPesquisa(numeroDaTarefa);

        Assert.assertEquals(mensagemErroEsperada, taskPage.retornarMensagemDeErro());
    }


    @Test
    public void realizarPesquisaPelaLupa_0043(){

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        taskPage = new TaskPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String numeroTarefa = "0000024";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmTarefas();
        taskPage.preencherERealizarPesquisa(numeroTarefa);

        Assert.assertEquals(numeroTarefa, taskPage.retornarIDDeTarefaPesquisada());
    }


    @Test
    public void adicionarAnotacaoPublicaATarefa_0044() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        taskPage = new TaskPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String numeroTarefa = "0000028";
        String anotacaoTexto = "Realizar uma anotação pública e realizar a asserção pela n.";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmTarefas();
        taskPage.clicarEmDeterminadaTarega(numeroTarefa);
        taskPage.preencherAnotacao(anotacaoTexto);
        taskPage.clicarEmAdicionarAnotacao();

        Assert.assertEquals(anotacaoTexto, taskPage.retornarDescricaoDaUltimaAnotacaoPreechida());
        Assert.assertFalse(taskPage.retornarInformacoesDaUltimaAnotacaoPreechida().contains("privado"));

    }

    @Test
    public void aadicionarAnotacaoPrivadaATarefa_0045() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        taskPage = new TaskPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String numeroTarefa = "0000028";
        String anotacaoTexto = "Realizar uma anotação pública e realizar a asserção pela n.";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmTarefas();
        taskPage.clicarEmDeterminadaTarega(numeroTarefa);
        taskPage.marcarNoCheckboxPrivado();
        taskPage.preencherAnotacao(anotacaoTexto);
        taskPage.clicarEmAdicionarAnotacao();

        Assert.assertEquals(anotacaoTexto, taskPage.retornarDescricaoDaUltimaAnotacaoPreechida());
        Assert.assertTrue(taskPage.retornarInformacoesDaUltimaAnotacaoPreechida().contains("privado"));

    }

    @Test
    public void mudarStatusTarefa_00046() {

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        taskPage = new TaskPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String numeroTarefa = "0000028";
        String statusFinal = "fechado";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmTarefas();
        taskPage.clicarEmDeterminadaTarega(numeroTarefa);
        taskPage.clicarEmAtualizar();
        taskPage.selecionarStatusNovo(statusFinal);
        taskPage.clicarEmAtualizarInformacao();

        Assert.assertEquals(statusFinal, taskPage.retornarStatusAtual());
    }

    @Test
    public void conferirLinhaDoTempo_00047() throws ParseException {

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        DateFormat patternTimeDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        myViewPage = new MyViewPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        Date first = null;
        Date last = null;

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmMinhaVisao();
        first = patternTimeDate.parse(myViewPage.retornaPrimeiroHorarioLista());
        last = patternTimeDate.parse(myViewPage.retornaUltimoHorarioLista());
        System.out.println(first);
        System.out.println(last);

        Assert.assertTrue(last.before(first));
    }


    @DataProvider(name = "dataTarefaCSVProvider")
    public Iterator<Object[]> dataTarefaProvider() {
        return Utils.csvProvider("src/test/java/dataTarefa.csv");
    }

    @Test(dataProvider = "dataTarefaCSVProvider")
    public void criarTarefaComDataDriven_0049(String[] tarefaData) {

        //Objects Instances
        loginFlows = new LoginFlows();
        taskFlows = new TaskFlows();
        mainPage = new MainPage();
        createTaskPage = new CreateTaskPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String projeto = "hello";
        String categoria = tarefaData[0];
        String frequencia = tarefaData[1];
        String gravidade = tarefaData[2];
        String prioridade = tarefaData[3];
        String resumo = tarefaData[4];
        String descricao = tarefaData[5];

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmCriarTarefa();
        createTaskPage.selecionarProjeto(projeto);
        createTaskPage.clicarEmSelecionarProjeto();
        taskFlows.criarTarefa(categoria, frequencia, gravidade, prioridade, resumo, descricao);

        Assert.assertEquals(categoria,  createTaskPage.retornarCategoriaPreenchida());
        Assert.assertEquals(frequencia, createTaskPage.retornarFrequenciaPreenchida());
        Assert.assertEquals(gravidade, createTaskPage.retornarGravidadePreenchida());
        Assert.assertEquals(prioridade, createTaskPage.retornarPrioridadePreenchida());
        Assert.assertEquals(descricao, createTaskPage.retornarDescricaoPreenchida());
        Assert.assertTrue(createTaskPage.retornarResumoPreenchida().endsWith(resumo));

    }


}