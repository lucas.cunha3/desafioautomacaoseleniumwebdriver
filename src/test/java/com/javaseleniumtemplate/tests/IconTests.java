package com.javaseleniumtemplate.tests;

import com.javaseleniumtemplate.bases.PageBase;
import com.javaseleniumtemplate.bases.TestBase;
import com.javaseleniumtemplate.flows.LoginFlows;
import com.javaseleniumtemplate.pages.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IconTests extends TestBase {

    MainPage mainPage;
    LoginFlows loginFlows;
    PageBase pageBase;
    PlanningPage planningPage;
    ChangeRecordPage changeRecordPage;
    ManageOverviewPage manageOverviewPage;
    ManageUsersPage manageUsersPage;

    @Test
    public void verificarCaminhoIconeMantis_0025(){
        //Objects Instances
        mainPage = new MainPage();
        loginFlows = new LoginFlows();
        pageBase = new PageBase();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String minhaVisaoTitulo = "Minha Visão - MantisBT";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarNoIconeMantis();

        Assert.assertEquals(minhaVisaoTitulo, pageBase.getTitle());

    }


    @Test
    public void verificarCaminhoResumo_0026(){
        //Objects Instances
        mainPage = new MainPage();
        loginFlows = new LoginFlows();
        pageBase = new PageBase();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String resumoTitulo = "Resumo - MantisBT";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmResumo();

        Assert.assertEquals(resumoTitulo, pageBase.getTitle());

    }


    @Test
    public void verificarPlanejamentoVazio_0027() {

        //Objects Instances
        mainPage = new MainPage();
        loginFlows = new LoginFlows();
        pageBase = new PageBase();
        planningPage = new PlanningPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String mensagem = "Nenhum planejamento disponível. Apenas tarefas com a versão indicada aparecerão no planejamento.";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmPlanejamento();

        Assert.assertEquals(mensagem, planningPage.retornarTextoMensagem());

    }

    @Test
    public void verificarRegistoDeMudancaVazio_0028() {

        //Objects Instances
        mainPage = new MainPage();
        loginFlows = new LoginFlows();
        changeRecordPage = new ChangeRecordPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String mensagem = "Nenhum registro de mudança disponível. Apenas tarefas que indiquem a versão na qual foi resolvida aparecerão nos registros de mudança.";

        //Test
        loginFlows.efetuarLogin(usuario, senha);
        mainPage.clicarEmRegistroDeMudancas();

        Assert.assertEquals(mensagem, changeRecordPage.retornaMensagem());

    }

    @Test
    public void conferirAtalhoCriarTarefa_0037() {

        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String linkDoIconeNoMenu = null;
        String linkDoAtalho = null;

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmCriarTarefa();
        linkDoIconeNoMenu = mainPage.getURL();
        System.out.println(linkDoIconeNoMenu);
        mainPage.clicarNoAtalhoDeCriarTarefa();
        linkDoAtalho = mainPage.getURL();
        System.out.println(linkDoAtalho);

        Assert.assertEquals(linkDoAtalho, linkDoIconeNoMenu);

    }

    @Test
    public void conferirAtalhoConvidarUsuarios_0038() {
        //Objects Instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        manageOverviewPage = new ManageOverviewPage();
        manageUsersPage = new ManageUsersPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String linkDoIconeNoMenuGerenciarUsuarios = null;
        String linkDoAtalho = null;

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarEmGerenciar();
        manageOverviewPage.clicarEmGerenciarUsuarios();
        manageUsersPage.clicarEmCriarNovaConta();
        linkDoIconeNoMenuGerenciarUsuarios = manageUsersPage.getURL();
        mainPage.clicarNoAtalhoDeCriarUsuario();
        linkDoAtalho = mainPage.getURL();

        Assert.assertEquals(linkDoIconeNoMenuGerenciarUsuarios, linkDoAtalho);

    }








}
