package com.javaseleniumtemplate.tests;

import com.javaseleniumtemplate.bases.TestBase;
import com.javaseleniumtemplate.flows.LoginFlows;
import com.javaseleniumtemplate.pages.LoginPage;
import com.javaseleniumtemplate.pages.LoginPasswordPage;
import com.javaseleniumtemplate.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;



public class LoginTests extends TestBase {
    //Objects
    LoginPage loginPage;
    LoginPasswordPage loginPasswordPage;
    MainPage mainPage;
    LoginFlows loginFlows;

    //Tests
    @Test
    public void efetuarLoginComSucesso_0001(){
        //Objects instances
        loginPage = new LoginPage();
        mainPage = new MainPage();
        loginPasswordPage = new LoginPasswordPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";

        //Test
        loginPage.preencherUsuario(usuario);
        loginPage.clicarEmEntrar();
        loginPasswordPage.preencherSenha(senha);
        loginPasswordPage.clicarEmEntrar();

        Assert.assertEquals(usuario, mainPage.retornaUsernameDasInformacoesDeLogin());


    }

    @Test
    public void efetuarLoginComFalha_0002(){
        //Objects instances
        loginPage = new LoginPage();
        mainPage = new MainPage();
        loginPasswordPage = new LoginPasswordPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrato";
        String mensagemErroEsperada = "Sua conta pode estar desativada ou bloqueada ou o nome de usuário e a senha que você digitou não estão corretos.";

        //Test
        loginPage.preencherUsuario(usuario);
        loginPage.clicarEmEntrar();
        loginPasswordPage.preencherSenha(senha);
        loginPasswordPage.clicarEmEntrar();

        Assert.assertEquals(mensagemErroEsperada, loginPage.retornaMensagemDeErro());

    }

    @Test
    public void efetuarLogoffComSucesso_0005() {
        //Objects instances
        loginPage = new LoginPage();
        mainPage = new MainPage();
        loginPasswordPage = new LoginPasswordPage();
        loginFlows = new LoginFlows();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";
        String entrar = "Entrar";

        //Test
        loginFlows.efetuarLogin(usuario,senha);
        mainPage.clicarNoUsuarioLogado();
        mainPage.clicarEmSair();

        Assert.assertEquals(entrar, loginPage.retornaMensagemEntrar());
    }

    //injeção de javascript
    @Test
    public void efetuarLoginComSucessoUtilizandoJS_0053(){
        //Objects instances
        loginPage = new LoginPage();
        mainPage = new MainPage();
        loginPasswordPage = new LoginPasswordPage();

        //Parameteres
        String usuario = "administrator";
        String senha = "administrator";

        //Test
        loginPage.preencherUsuarioComJS(usuario);
        loginPage.clicarEmEntrarcomJS();
        loginPasswordPage.preencherSenhaComJS(senha);
        loginPasswordPage.clicarEmEntrarComJS();

        Assert.assertEquals(usuario, mainPage.retornaUsernameDasInformacoesDeLogin());

    }


}
