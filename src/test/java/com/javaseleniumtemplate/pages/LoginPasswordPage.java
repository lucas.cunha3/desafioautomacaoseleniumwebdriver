package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class LoginPasswordPage extends PageBase {
    //Mapping
    By passwordField = By.id("password");
    By entrarButton = By.xpath("//input[@value=\"Entrar\"]");

    //Actions
    public void preencherSenha(String senha){ sendKeys(passwordField, senha); }

    public void clicarEmEntrar() { click(entrarButton); }

    public void preencherSenhaComJS(String senha){ SendKeysJavaScript(passwordField, senha); }

    public void clicarEmEntrarComJS() { ClickJavaScript(entrarButton); }


}
