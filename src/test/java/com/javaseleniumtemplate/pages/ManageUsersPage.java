package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class ManageUsersPage extends PageBase {
    //Mapping
    By createNewAccountButton = By.linkText("Criar nova conta");

    By userNameField = By.id("user-username");

    By realNameField = By.id("user-realname");

    By emailField = By.id("email-field");

    By levelOfAccessCombobox = By.id("user-access-level");

    By createUserButton = By.xpath("//input[@value=\"Criar Usuário\"]");

    By messageSuccessText = By.xpath("//p[@class=\"bold bigger-110\"]");

    By messageErrorText = By.xpath("//div[@class=\"alert alert-danger\"]");

    By selectUser(String selecionarUsuario) { return By.xpath("//a[contains(text(),'" + selecionarUsuario + "')]");}

    By deleteUserButton = By.id("manage-user-delete-form");

    By secondDeleteUserButton = By.xpath("//input[@value=\"Apagar Conta\"]");

    By editLevelOfAccessCombobox = By.id("edit-access-level");

    By updateUserButton = By.xpath("//input[@value=\"Atualizar Usuário\"]");

    By selectLetterButton(String letra){ return By.xpath("//div[@class=\"btn-group\"]//*[text()=\"" + letra + "\"]");}

    By checkSelectedLetter = By.xpath("//a[@class=\"btn btn-xs btn-white btn-primary active\"]");

    By enableUserCheckbox = By.xpath("//div[@id=\"edit-user-div\"]//tr[5]//span");

    By infoEnableUserCheckbox = By.xpath("//div[@id=\"edit-user-div\"]//tr[5]//input");

    By updatePreferencesButton = By.xpath("//input[@value=\"Atualizar Usuário\"]");

    By showDisabledCheckbox = By.xpath("//span[contains(text(),'Mostrar desativados')]");

    By applyFilterButton = By.xpath("//input[@value=\"Aplicar Filtro\"]");


    //Actions
    public void clicarEmCriarNovaConta(){click(createNewAccountButton);}

    public void preencherNomeDeUsuario(String nomeUsuario){sendKeys(userNameField, nomeUsuario);}

    public void preencherNomeRealDeUsuario(String nomeRealUsuario){sendKeys(realNameField, nomeRealUsuario);}

    public void preencherEmailDeUsuario(String emailUsuario){sendKeys(emailField, emailUsuario);}

    public void selecionarNivelDeAcessoDoUsuario (String nivelDeAcesso){ comboBoxSelectByVisibleText(levelOfAccessCombobox, nivelDeAcesso);}

    public void alterarNivelDeAcessoDoUsuario (String nivelDeAcesso){ comboBoxSelectByVisibleText(editLevelOfAccessCombobox, nivelDeAcesso);}

    public void clicarEmCriarNovoUsuario(){click(createUserButton);}

    public String retornarMensagemDeSucesso(){return getText(messageSuccessText);}

    public String retornaMensagemDeErro(){return getText(messageErrorText);}

    public void clicarApagarUsuario(){click(deleteUserButton); click(secondDeleteUserButton);}

    public void clicarEmDeterminadoUsuario(String nomeUsuario){ click(selectUser(nomeUsuario));}

    public boolean  verificarSeUsuarioExiste(String nomeUsuario) {return returnIfElementExists(selectUser(nomeUsuario));}

    public String retornarOpcaoSelecionadaDeNivelDeAcesso(){return comboBoxSelectedOption(editLevelOfAccessCombobox);}

    public void clicarEmAtualizarUsuario(){click(updateUserButton);}

    public void clicarEmUmaLetra(String letra){click(selectLetterButton(letra));}

    public String retornarLetraSelecionada(){ return getText(checkSelectedLetter);}

    public void clicarNoCheckBoxHabilitar(){click(enableUserCheckbox);}

    public void clicarEmAtualizarPreferencias(){click(updatePreferencesButton);}

    public String retornarSeBotaoHabilitarEstaMarcado(){return getChecked(infoEnableUserCheckbox);}

    public void marcarEmMostrarDesativados(){click(showDisabledCheckbox);}

    public void clicarEmAplicarFiltro(){click(applyFilterButton);}



}
