package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TaskPage extends PageBase {

    //Mapping
    By selectTaskById(String numero){ return By.xpath("//table//a[contains(text(),'" + numero + "')]");}

    By deleteTaskButton = By.xpath("//input[@value=\"Apagar\"]");

    By deleteButton = By.xpath("//input[@value=\"Apagar Tarefas\"]");

    By lastRecentlyVisitedText = By.xpath("//div[@class=\"nav-recent hidden-xs\"]//a[1]");

    By searchField = By.xpath("//input[@name=\"bug_id\"]");

    By ErrorMessageText = By.xpath("//div[@class=\"alert alert-danger\"]");

    By returnedSearchIDText = By.xpath("//td[@class=\"bug-id\"]");

    By bugnoteField = By.id("bugnote_text");

    By addAnnotationButton = By.xpath("//input[@value=\"Adicionar Anotação\"]");

    By lastNoteInfoText = By.cssSelector("div#bugnotes tr:nth-last-child(2) p:nth-child(2)");

    By lastAnnotationDescriptionText = By.cssSelector("div#bugnotes tr:nth-last-child(2) td:nth-child(2)");

    By privateAnnotationCheckbox = By.xpath("//*[@for=\"bugnote_add_view_status\"]//span");

    By updateButton = By.xpath("//input[@value=\"Atualizar\"]");

    By statusComboBox = By.id("status");

    By updateInfoButton = By.xpath("//input[@value=\"Atualizar Informação\"]");

    By statusInfoText = By.xpath("//*[@class=\"bug-status\"]");



    //Actions
    public void clicarEmDeterminadaTarega (String numeroTarefa){ click(selectTaskById(numeroTarefa));}

    public void clicarEmDeletarTarefa(){click(deleteTaskButton);}

    public void clicarEmDeletar(){click(deleteButton);}

    public boolean retornaSeTarefaExiste(String numeroTarefa){return returnIfElementExists(selectTaskById(numeroTarefa));}

    public String retornaUltimaTarefaVisitada(){ return getText(lastRecentlyVisitedText);}

    public void preencherERealizarPesquisa(String numeroTarefa){sendKeys(searchField, numeroTarefa); submit(searchField);}

    public String retornarMensagemDeErro() {return getText(ErrorMessageText);}

    public String retornarIDDeTarefaPesquisada(){return getText(returnedSearchIDText);}

    public void preencherAnotacao(String anotacao){sendKeys(bugnoteField, anotacao);}

    public void clicarEmAdicionarAnotacao(){ click(addAnnotationButton);}

    public String retornarInformacoesDaUltimaAnotacaoPreechida(){return getText(lastNoteInfoText);}

    public String retornarDescricaoDaUltimaAnotacaoPreechida(){return getText(lastAnnotationDescriptionText);}

    public void marcarNoCheckboxPrivado(){click(privateAnnotationCheckbox);}

    public void clicarEmAtualizar(){click(updateButton);}

    public void selecionarStatusNovo(String status){comboBoxSelectByVisibleText(statusComboBox, status);}

    public void clicarEmAtualizarInformacao(){click(updateInfoButton);}

    public String retornarStatusAtual(){return getText(statusInfoText);}

}
