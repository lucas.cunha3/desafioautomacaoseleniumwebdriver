package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class ManageOverviewPage extends PageBase {
    //Mapping
    By infoVersionMantis = By.cssSelector("tr:nth-child(1) td");

    By infoVersionSchema = By.cssSelector("tr:nth-child(2) td");

    By manageProjectsButton = By.xpath("//a[@href=\"/manage_proj_page.php\"]");

    By manageTagsButton = By.xpath("//a[@href=\"/manage_tags_page.php\"]");

    By manageGlobalProfilesButton = By.xpath("//a[contains(text(),\"Gerenciar Perfís Globais\")]");

    By manageUsersButton = By.xpath("//a[contains(text(),\"Gerenciar Usuários\")]");

    By managePluginsButton = By.xpath("//a[contains(text(),'Gerenciar Plugins')]");


    //Actions
    public String retornaVersaoMantis(){return getText(infoVersionMantis);}

    public String retornaVersaoSchema(){return getText(infoVersionSchema);}

    public void clicarEmGerenciarProjetos(){click(manageProjectsButton);}

    public void clicarEmGerenciarMarcadores(){ click(manageTagsButton);}

    public void clicarEmGerenciarPerfisGlobais(){ click(manageGlobalProfilesButton);}

    public void clicarEmGerenciarUsuarios(){click(manageUsersButton);}

    public void clicarEmGerenciarPlugins(){click(managePluginsButton);}
}
