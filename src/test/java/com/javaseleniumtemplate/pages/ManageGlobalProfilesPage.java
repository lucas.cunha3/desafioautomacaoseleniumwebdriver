package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class ManageGlobalProfilesPage extends PageBase {
    //Mapping
    By plataformField = By.xpath("//*[@name=\"platform\"]");

    By osField = By.xpath("//*[@name=\"os\"]");

    By versionOSField = By.xpath("//*[@name=\"os_build\"]");

    By descriptionField = By.xpath("//*[@name=\"description\"]");

    By addGlobalProfileButton = By.xpath("//input[@value=\"Adicionar Perfil\"]");

    By verifyIfGlobalProfileExists (String nomePerfilGlobal){ return By.xpath("//select[@class=\"input-sm\"]//option[contains(text(),\"" + nomePerfilGlobal + "\")]");}

    By selectGlobalProfile = By.xpath("//select[@class=\"input-sm\"]");

    By deleteProfileCheckBox = By.xpath("//tr[2]//span[@class=\"lbl\"]");

    By sendButton = By.xpath("//input[@value=\"Enviar\"]");

    By editProfileCheckBox = By.xpath("//tr[1]//span[@class=\"lbl\"]");

    By updateProfileButton =  By.xpath("//*[@value=\"Atualizar Perfil\"]");

    //Actions

    public void preencherPlataforma( String nomePlataforma){ clearAndSendKeys(plataformField,nomePlataforma);}

    public void preencherOS( String nomeOS){ clearAndSendKeys(osField,nomeOS);}

    public void preencherVersaoOS( String nomeVersaoOS){ clearAndSendKeys(versionOSField,nomeVersaoOS);}

    public void preencherDescricao( String nomeDescricao){ clearAndSendKeys(descriptionField,nomeDescricao);}

    public void clicarEmAdicionarPerfilGlobal() { click(addGlobalProfileButton);}

    public String retornarNomePerfil(String nomePerfilGlobalExistente){ return getText(verifyIfGlobalProfileExists(nomePerfilGlobalExistente));}

    public void selecionarApagarPerfil(){click(deleteProfileCheckBox);}

    public void selecionarPerfilGlobalNaLista(String nomePerfilGlobal){ comboBoxSelectByVisibleText(selectGlobalProfile, nomePerfilGlobal);}

    public void clicarEmEnviar(){click(sendButton);}

    public boolean verificarSePerfilExiste(String nomePerfilGlobal){return returnIfElementExists(verifyIfGlobalProfileExists(nomePerfilGlobal));}

    public void selecionarEditarPerfil(){click(editProfileCheckBox);}

    public void clicarEmAtualizarPerfilGlobal(){click(updateProfileButton);}




}
