package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class CreateTaskPage extends PageBase {
    //Mapping
    By selectProjectCombobox = By.id("select-project-id");

    By selectProjectButton = By.xpath("//input[@value=\"Selecionar Projeto\"]");

    By categoryCombobox = By.id("category_id");

    By frequencyCombobox = By.id("reproducibility");

    By severityCombobox = By.id("severity");

    By priorityCombobox = By.id("priority");

    By summaryField = By.id("summary");

    By descriptionField = By.id("description");

    By createNewTaskButton = By.xpath("//input[@value=\"Criar Nova Tarefa\"]");

    By filledCategory = By.xpath("//td[@class=\"bug-category\"]");

    By filledFrequency = By.xpath("//td[@class=\"bug-reproducibility\"]");

    By filledSeverity = By.xpath("//td[@class=\"bug-severity\"]");

    By filledPriority = By.xpath("//td[@class=\"bug-priority\"]");

    By filledSummary = By.xpath("//td[@class=\"bug-summary\"]");

    By filledDescription = By.xpath("//td[@class=\"bug-description\"]");


    //Actions
    public void selecionarProjeto(String projeto){comboBoxSelectByVisibleText(selectProjectCombobox, projeto);}

    public void clicarEmSelecionarProjeto(){click(selectProjectButton);}

    public void selecionarCategoria(String categoria){comboBoxSelectByVisibleText(categoryCombobox, categoria);}

    public void selecionarFrequencia(String frequencia){comboBoxSelectByVisibleText(frequencyCombobox, frequencia);}

    public void selecionarGravidade(String gravidade){comboBoxSelectByVisibleText(severityCombobox,gravidade);}

    public void selecionarPrioridade(String prioridade){comboBoxSelectByVisibleText(priorityCombobox, prioridade);}

    public void preencherResumo(String sumario){sendKeys(summaryField, sumario);}

    public void preencherDescricao(String descricao){sendKeys(descriptionField, descricao);}

    public void clicarEmCriarNovaTarefa(){click(createNewTaskButton);}

    public String retornarCategoriaPreenchida(){return getText(filledCategory);}

    public String retornarFrequenciaPreenchida(){return getText(filledFrequency);}

    public String retornarGravidadePreenchida(){return getText(filledSeverity);}

    public String retornarPrioridadePreenchida(){return getText(filledPriority);}

    public String retornarResumoPreenchida(){return getText(filledSummary);}

    public String retornarDescricaoPreenchida(){return getText(filledDescription);}






}
