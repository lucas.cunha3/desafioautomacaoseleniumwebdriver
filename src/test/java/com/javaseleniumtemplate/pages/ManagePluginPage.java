package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class ManagePluginPage extends PageBase {
    //Mapping
    By selectPluginToInstallByName(String nomePlugin){ return By.xpath("//td[position()=count(//td[normalize-space(.)='Plugin']/preceding-sibling::th)+1 and normalize-space(.)='" + nomePlugin + "']/ancestor::tr//a[contains(text(),'Instalar')]");}

    By selectPluginToDesinstallByName(String nomePlugin){ return By.xpath("//td[position()=count(//td[normalize-space(.)='Plugin']/preceding-sibling::th)+1 and normalize-space(.)='" + nomePlugin + "']/ancestor::tr//a[contains(text(),'Desinstalar')]");}

    By installedPluginNameText (String nomePlugin){return By.xpath("//div[@class=\"form-container\"]//*[contains(text(),'" + nomePlugin + "')]");}

    By desinstallButton = By.xpath("//input[@value=\"Desinstalar\"]");


    //Actions
    public void clicarEmInstalarPlugin(String nomePlugin){click(selectPluginToInstallByName(nomePlugin));}

    public void clicarEmDesinstalarPlugin(String nomePlugin){click(selectPluginToDesinstallByName(nomePlugin));}

    public String retornarNomeDoPlugin(String nomePlugin){return getText(installedPluginNameText(nomePlugin));}

    public void clicarNoBotaoDesinstalar(){click(desinstallButton);}

    public boolean retornarSePluginEstaInstalado (String nomePlugin) {return returnIfElementExists(installedPluginNameText(nomePlugin));}



}
