package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class MainPage extends PageBase {
    //Mapping
    By usernameLoginInfoTextArea = By.className("user-info");

    String className = "user-info";

    By gerenciarButton = By.xpath("//*[@class=\"menu-icon fa fa-gears\"]");

    By logoffButton = By.xpath("//i[@class=\"ace-icon fa fa-sign-out\"]");

    By myAccountButton = By.xpath("//i[@class=\"ace-icon fa fa-user\"]");

    By mantisIconButton = By.className("smaller-75");

    By resumeButton = By.xpath("//*[@class=\"menu-icon fa fa-bar-chart-o\"]");

    By planningButton = By.xpath("//i[@class=\"menu-icon fa fa-road\"]");

    By changeRecordButton = By.xpath("//i[@class=\"menu-icon fa fa-retweet\"]");

    By createTaskButton = By.xpath("//*[@class=\"menu-icon fa fa-edit\"]");

    By createTaskShortcutButton = By.xpath("//a[contains(text(),'Criar Tarefa')]");

    By taskButton = By.xpath("//*[@class=\"menu-icon fa fa-list-alt\"]");

    By myVisionButton = By.xpath("//i[@class=\"menu-icon fa fa-dashboard\"]");

    By createUserShortcutButton = By.xpath("//a[@class=\"btn btn-primary btn-sm\"][2]");



    //Actions
    public String retornaUsernameDasInformacoesDeLogin(){
        return getText(usernameLoginInfoTextArea);
    }

    public void clicarEmGerenciar() { click(gerenciarButton); }

    public void clicarNoUsuarioLogado(){ click(usernameLoginInfoTextArea);}

    public void clicarEmSair(){click(logoffButton);}

    public void clicarEmMinhaConta(){click(myAccountButton);}

    public void clicarNoIconeMantis(){click(mantisIconButton);}

    public void clicarEmResumo(){click(resumeButton);}

    public void clicarEmPlanejamento(){click(planningButton);}

    public void clicarEmRegistroDeMudancas(){click(changeRecordButton);}

    public void clicarEmCriarTarefa(){click(createTaskButton);}

    public void clicarNoAtalhoDeCriarTarefa(){click(createTaskShortcutButton);}

    public void clicarEmTarefas(){click(taskButton);}

    public void clicarEmMinhaVisao(){click(myVisionButton);}

    public void clicarNoAtalhoDeCriarUsuario(){click(createUserShortcutButton);}










    //By reportIssueLink = By.xpath("//a[@href='/bug_report_page.php']");
    /* public void clicarEmReportIssue(){
        click(reportIssueLink);
    }

     */
}
