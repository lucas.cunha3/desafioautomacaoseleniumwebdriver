package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class ManageProjectsPage extends PageBase {
    //Mapping
    By createProjectButton = By.xpath("//*[@class=\"btn btn-primary btn-white btn-round\"]");

    By nameNewProjectField = By.id("project-name");

    By statusNewProjectField = By.id("project-status");

    By visibilityNewProjectField = By.id("project-view-state");

    By descriptionNewProjectField = By.id("project-description");

    By addNewProjectButton = By.xpath("//input[@class=\"btn btn-primary btn-white btn-round\"]");

    By messageSuccessText = By.xpath("//p[@class=\"bold bigger-110\"]");

    By titleTableCreateProject = By.xpath("//h4[@class=\"widget-title lighter\"]");

    By selectProjectByNameField(String nameProject){ return By.xpath("//td//a[contains(text(),\"" + nameProject + "\")]");}

    By deleteProjectButton = By.xpath("//input[@value=\"Apagar Projeto\"]");

    By nameCategoryField = By.className("input-sm");

    By addEditCategoryButton = By.xpath("//input[@name=\"add_and_edit_category\"]");

    By assignToComboBox = By.id("proj-category-assigned-to");

    By updateCategoryButton = By.xpath("//input[@value=\"Atualizar Categoria\"]");

    By ErrorMessageText = By.xpath("//div[@class=\"alert alert-danger\"]");

    By selectUsersComboBox = By.id("project-add-users-username");

    By addUserToProjectButton = By.xpath("//input[@value=\"Adicionar Usuário\"]");

    By verifyUserInProject (String name){ return By.xpath("//td[@data-sortvalue=\"" + name + "\"]");}

    By revogeAccessUser(String usuario){ return By.xpath("//div[@class=\"widget-box widget-color-blue2\"]//td[position()=count(//th[normalize-space(.)='Nome de usuário']/preceding-sibling::th)+1 and normalize-space(.)='" + usuario + "']/ancestor::tr//td[position()=count(//div[@class=\"widget-box widget-color-blue2\"]//th[normalize-space(.)='Remover']/preceding-sibling::th)+1]//span");}

    By applyChangesButton = By.xpath("//input[@value=\"Aplicar mudanças\"]");

    By confirmChangesButton = By.xpath("//input[@value=\"Sim\"]");

    By levelOfAccessCombobox = By.id("project-add-users-access-level");

    //Actions
    public void clicarEmCriarProjeto(){ click(createProjectButton); }

    public void inserirNomeNovoProjeto(String nomeProjetoNovo){sendKeys(nameNewProjectField, nomeProjetoNovo);}

    public void definirStatusNovoProjeto(String statusProjetoNovo){comboBoxSelectByVisibleText(statusNewProjectField, statusProjetoNovo);}

    public void inserirDescricaoNovoProjeto(String descricaoProjetoNovo){sendKeys(descriptionNewProjectField, descricaoProjetoNovo);}

    public void definirVisibilidadeNovoProjeto(String visibilidadeProjetoNovo){comboBoxSelectByVisibleText(visibilityNewProjectField, visibilidadeProjetoNovo);}

    public void clicarEmAdicionarProjeto(){click(addNewProjectButton);}

    public String retornaMensagemSucesso(){return getText(messageSuccessText);}

    public String retornaMensagemErro(){return getText(ErrorMessageText);}

    public String retornaTituloDaTabela(){return getText(titleTableCreateProject);}

    public void clicarEmProjeto(String nomeProjeto){ click(selectProjectByNameField(nomeProjeto)); }

    public void apagarProjeto(){ click(deleteProjectButton); click(deleteProjectButton);}

    public boolean retornaSeOProjetoExisteNaTabela (String nomeProjeto) { return returnIfElementExists(selectProjectByNameField(nomeProjeto)); }

    public void preencherNomeCategoria(String nomeCategoria){sendKeys(nameCategoryField, nomeCategoria);}

    public void clicarAdicionarEEditarCategoria(){click(addEditCategoryButton);}

    public void atribuirCategoriaAUsuario(String usuario){comboBoxSelectByVisibleText(assignToComboBox, usuario);}

    public void clicarEmAtualizarCategoria(){click(updateCategoryButton);}

    public void selecionarUsuarioParaAdicionarAProjetoPrivado(String usuario){comboBoxSelectByVisibleText(selectUsersComboBox , usuario);}

    public void adicionarUsuarioAProjetoPrivado(){click(addUserToProjectButton);}

    public boolean verificarSeUsuarioFoiAdicionadoAProjeto(String nomeUsuario){ return returnIfElementExists(verifyUserInProject(nomeUsuario));}

    public void clicarEmRevogarAcessoDeDeterminadoUsuario (String user){click(revogeAccessUser(user));}

    public void clicarEmAplicarMudancas(){click(applyChangesButton);}

    public void clicarEmConfirmarMudancas(){click(confirmChangesButton);}

    public void selecionarNivelDeAcessoDoUsuario (String nivelDeAcesso){ comboBoxSelectByVisibleText(levelOfAccessCombobox, nivelDeAcesso);}









}
