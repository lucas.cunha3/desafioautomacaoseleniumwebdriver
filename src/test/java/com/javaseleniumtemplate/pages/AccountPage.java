package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class AccountPage extends PageBase {

    //Mapping
    By tokensAPIButton = By.xpath("//a[contains(text(),'Tokens API')]");

    By nameNewAPIField = By.id("token_name");

    By createAPIButton = By.xpath("//input[@value=\"Criar Token API\"]");

    By messageTokenCreated = By.xpath("//div[@class=\"lead red\"]");

    By messageErrorToken = By.xpath("//div[@class=\"alert alert-danger\"]");

    By revogeAccessToken(String nameToken){ return By.xpath("//table[@class=\"table table-bordered table-condensed table-striped\"]//td[position()=count(//th[normalize-space(.)='Nome do Token']/preceding-sibling::th)+1 and normalize-space(.)='" + nameToken + "']/ancestor::tr//td[position()=count(//table[@class=\"table table-bordered table-condensed table-striped\"]//th[normalize-space(.)='Ações']/preceding-sibling::th)+1]//input[@value='Revogar']");}

    By revokedTokenMessage = By.xpath("//div[@class=\"lead\"]");

    By preferencesButton = By.partialLinkText("Preferências");

    By timezoneCombobox = By.id("timezone");

    By savePreferencesButton = By.xpath("//input[@value=\"Atualizar Preferências\"]");

    By timeRenovationField = By.id("refresh-delay");

    By manageColumnsButton = By.xpath("//a[contains(text(),'Gerenciar Colunas')]");

    By resetColumnSettingsButton = By.xpath("//input[@value=\"Reinicializar Configurações das Colunas\"]");

    By messageSuccessText = By.xpath("//p[@class=\"bold bigger-110\"]");


    //Actions
    public void clicarEmTokensAPI(){click(tokensAPIButton);}

    public void inserirNomeAPI(String nomeAPI) {sendKeys(nameNewAPIField,nomeAPI);}

    public void clicarEmCriarAPI(){click(createAPIButton);}

    public String retornarMensagemDeTokenGerado(){ return getText(messageTokenCreated);}

    public String retornaMensagemDeErroToken(){return getText(messageErrorToken);}

    public void clicarEmRevogarAcesso(String nomeAPIRevogar){click(revogeAccessToken(nomeAPIRevogar));}

    public String retornarMensagemDeTokenRevogado(){return getText(revokedTokenMessage);}

    public void clicarEmPreferencias(){click(preferencesButton);}

    public void selecionarFusoHorario(String fusohorario){comboBoxSelectByVisibleText(timezoneCombobox, fusohorario);}

    public void clicarEmSalvarPreferencias(){ click(savePreferencesButton);}

    public String retornarFusoHorarioSelecionado(){return comboBoxSelectedOption(timezoneCombobox);}

    public void alterarTempoDeRenovacao(String tempoDeRenovacao){clearAndSendKeys(timeRenovationField, tempoDeRenovacao);}

    public String retornarTempoDeRenovacaoSalvo(){return getValue(timeRenovationField);}

    public void clicarEmGerenciarColunas(){click(manageColumnsButton);}

    public void clicarEmReinicializarConfiguracoesDasColunas(){click(resetColumnSettingsButton);}

    public String retornaMensagemSucesso(){return getText(messageSuccessText);}






}



