package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class ChangeRecordPage extends PageBase {

    //Mapping
    By messageText = By.className("lead");

    //Actions
    public String retornaMensagem(){return getText(messageText);}

}
