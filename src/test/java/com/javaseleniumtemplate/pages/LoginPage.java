package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class LoginPage extends PageBase {
    //Mapping
    By usernameField = By.id("username");
    By entrarButton = By.xpath("//input[@value=\"Entrar\"]");
    By mensagemDeErroTextArea = By.xpath("//div[@class=\"alert alert-danger\"]");
    By mensagemDaPaginaLogin = By.xpath("//h4[@class=\"header lighter bigger\"]");


    //Actions

    public void preencherUsuario(String usuario){
        sendKeys(usernameField, usuario);
    }

    public void clicarEmEntrar() { click(entrarButton); }

    public String retornaMensagemDeErro() { return getText(mensagemDeErroTextArea); }

    public String retornaMensagemEntrar(){return getText(mensagemDaPaginaLogin);}

    public void preencherUsuarioComJS(String usuario){
        SendKeysJavaScript(usernameField, usuario);
    }

    public void clicarEmEntrarcomJS() { ClickJavaScript(entrarButton); }






}
