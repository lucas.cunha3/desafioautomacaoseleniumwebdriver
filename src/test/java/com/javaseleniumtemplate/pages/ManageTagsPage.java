package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class ManageTagsPage extends PageBase {
    //Mapping

    By createTagsButton = By.xpath("//a[@href=\"#tagcreate\"]");

    By nameTagField = By.id("tag-name");

    By descriptionTagField = By.id("tag-description");

    By createNewTagButton = By.xpath("//input[@name=\"config_set\"]");

    By verifyTagByName(String nameTag){ return By.xpath("//a[contains(text(),\"" + nameTag + "\")]");}

    By selectTag(String marcador){return By.xpath("//a[contains(text(),\"" + marcador + "\")]");}

    By deleteTagButton = By.xpath("//input[@value=\"Apagar Marcador\"]");


    //Actions

    public void clicarEmCriarMarcador(){click(createTagsButton);}

    public void preencherNomeMarcador(String nomeNovoMarcador){ sendKeys(nameTagField, nomeNovoMarcador);}

    public void preencherDescricaoMarcador(String descricaoNovoMarcador){ sendKeys(descriptionTagField, descricaoNovoMarcador);}

    public void clicarEmCriarNovoMarcador(){click(createNewTagButton);}

    public boolean retornaSeMarcadorFoiCriado( String nomeNovoMarcador){return returnIfElementExists(verifyTagByName(nomeNovoMarcador));}

    public void clicarEmApagarMarcador(){click(deleteTagButton);click(deleteTagButton);}

    public void clicarEmDeterminadoMarcador(String marcador){click(selectTag(marcador));}

    public boolean verificarSeMarcadorExiste(String marcador){return returnIfElementExists(selectTag(marcador));}


}
