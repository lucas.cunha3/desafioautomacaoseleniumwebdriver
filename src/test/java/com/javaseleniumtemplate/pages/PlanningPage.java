package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class PlanningPage extends PageBase {

    //Mapping

    By messageText = By.className("lead");

    //Actions
    public String retornarTextoMensagem(){return getText(messageText);}

}
