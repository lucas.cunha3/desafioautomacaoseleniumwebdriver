package com.javaseleniumtemplate.pages;

import com.javaseleniumtemplate.bases.PageBase;
import org.openqa.selenium.By;

public class MyViewPage extends PageBase {

    //Mapping

    By selectUnassignedProject (String numeroProjeto){return By.xpath("//*[@id=\"unassigned\"]//a[contains(text(),\"" + numeroProjeto + "\")]");}

    By updateButton = By.xpath("//input[@value=\"Atualizar\"]");

    By assignUserCombobox = By.id("handler_id");

    By updateInfoButton = By.xpath("//input[@value=\"Atualizar Informação\"]");

    By assingUserText = By.xpath("//*[@class=\"bug-assigned-to\"]/a");

    By timesInTheList = By.xpath("//div[@class=\"time\"]");


    //Actions
    public void clicarEmDeterminadoProjetoNaoAtribuido(String numeroProjeto){click(selectUnassignedProject(numeroProjeto));}

    public void clicarEmAtualizar(){click(updateButton);}

    public void selecionarUsuarioParaAtribuirTarefa(String usuarioAtribuido){comboBoxSelectByVisibleText(assignUserCombobox, usuarioAtribuido);}

    public void clicarEmAtualizarInformacoes(){click(updateInfoButton);}

    public String retornarNomeUsuarioAtribuido(){return getText(assingUserText);}

    public String retornaPrimeiroHorarioLista(){return firstElementOfAList(timesInTheList);}

    public String retornaUltimoHorarioLista(){return lastElementOfAList(timesInTheList);}


}
