-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           10.6.5-MariaDB-1:10.6.5+maria~focal - mariadb.org binary distribution
-- OS do Servidor:               debian-linux-gnu
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para bugtracker
CREATE DATABASE IF NOT EXISTS `bugtracker` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `bugtracker`;

-- Copiando estrutura para tabela bugtracker.mantis_api_token_table
CREATE TABLE IF NOT EXISTS `mantis_api_token_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL,
  `hash` varchar(128) NOT NULL,
  `date_created` int(10) unsigned NOT NULL DEFAULT 1,
  `date_used` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_id_name` (`user_id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_api_token_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_api_token_table`;
/*!40000 ALTER TABLE `mantis_api_token_table` DISABLE KEYS */;
INSERT INTO `mantis_api_token_table` (`id`, `user_id`, `name`, `hash`, `date_created`, `date_used`) VALUES
	(19, 1, 'Teste API no Template Base2', '5f4a66389f993cdbc3f4cde3f7a45b6ec9a4d392c493fb90684f67a73230bef9', 1646834328, 1);
/*!40000 ALTER TABLE `mantis_api_token_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bugnote_table
CREATE TABLE IF NOT EXISTS `mantis_bugnote_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bug_id` int(10) unsigned NOT NULL DEFAULT 0,
  `reporter_id` int(10) unsigned NOT NULL DEFAULT 0,
  `bugnote_text_id` int(10) unsigned NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `note_type` int(11) DEFAULT 0,
  `note_attr` varchar(250) DEFAULT '',
  `time_tracking` int(10) unsigned NOT NULL DEFAULT 0,
  `last_modified` int(10) unsigned NOT NULL DEFAULT 1,
  `date_submitted` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idx_bug` (`bug_id`),
  KEY `idx_last_mod` (`last_modified`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bugnote_table: ~9 rows (aproximadamente)
DELETE FROM `mantis_bugnote_table`;
/*!40000 ALTER TABLE `mantis_bugnote_table` DISABLE KEYS */;
INSERT INTO `mantis_bugnote_table` (`id`, `bug_id`, `reporter_id`, `bugnote_text_id`, `view_state`, `note_type`, `note_attr`, `time_tracking`, `last_modified`, `date_submitted`) VALUES
	(11, 25, 1, 11, 50, 0, '', 0, 1646760308, 1646760308),
	(12, 26, 1, 12, 10, 0, '', 0, 1646760378, 1646760378),
	(13, 25, 1, 13, 50, 0, '', 0, 1646760438, 1646760438),
	(14, 26, 1, 14, 10, 0, '', 0, 1646763712, 1646763712),
	(15, 25, 1, 15, 50, 0, '', 0, 1646763726, 1646763726),
	(16, 25, 1, 16, 50, 0, '', 0, 1646833766, 1646833766),
	(17, 26, 1, 17, 10, 0, '', 0, 1646833776, 1646833776),
	(18, 28, 1, 18, 10, 0, '', 0, 1646856755, 1646856755),
	(19, 28, 1, 19, 50, 0, '', 0, 1646856766, 1646856766);
/*!40000 ALTER TABLE `mantis_bugnote_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bugnote_text_table
CREATE TABLE IF NOT EXISTS `mantis_bugnote_text_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `note` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bugnote_text_table: ~9 rows (aproximadamente)
DELETE FROM `mantis_bugnote_text_table`;
/*!40000 ALTER TABLE `mantis_bugnote_text_table` DISABLE KEYS */;
INSERT INTO `mantis_bugnote_text_table` (`id`, `note`) VALUES
	(11, 'Realizar uma anotação pública e realizar a asserção pela n.'),
	(12, 'Realizar uma anotação pública e realizar a asserção pela n.'),
	(13, 'Realizar uma anotação pública e realizar a asserção pela n.'),
	(14, 'Realizar uma anotação pública e realizar a asserção pela n.'),
	(15, 'Realizar uma anotação pública e realizar a asserção pela n.'),
	(16, 'Realizar uma anotação pública e realizar a asserção pela n.'),
	(17, 'Realizar uma anotação pública e realizar a asserção pela n.'),
	(18, 'Realizar uma anotação pública e realizar a asserção pela n.'),
	(19, 'Realizar uma anotação pública e realizar a asserção pela n.');
/*!40000 ALTER TABLE `mantis_bugnote_text_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bug_file_table
CREATE TABLE IF NOT EXISTS `mantis_bug_file_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bug_id` int(10) unsigned NOT NULL DEFAULT 0,
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `diskfile` varchar(250) NOT NULL DEFAULT '',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `folder` varchar(250) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT 0,
  `file_type` varchar(250) NOT NULL DEFAULT '',
  `content` longblob DEFAULT NULL,
  `date_added` int(10) unsigned NOT NULL DEFAULT 1,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `idx_bug_file_bug_id` (`bug_id`),
  KEY `idx_diskfile` (`diskfile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bug_file_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_bug_file_table`;
/*!40000 ALTER TABLE `mantis_bug_file_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_bug_file_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bug_history_table
CREATE TABLE IF NOT EXISTS `mantis_bug_history_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `bug_id` int(10) unsigned NOT NULL DEFAULT 0,
  `field_name` varchar(64) NOT NULL,
  `old_value` varchar(255) NOT NULL,
  `new_value` varchar(255) NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT 0,
  `date_modified` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idx_bug_history_bug_id` (`bug_id`),
  KEY `idx_history_user_id` (`user_id`),
  KEY `idx_bug_history_date_modified` (`date_modified`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bug_history_table: ~41 rows (aproximadamente)
DELETE FROM `mantis_bug_history_table`;
/*!40000 ALTER TABLE `mantis_bug_history_table` DISABLE KEYS */;
INSERT INTO `mantis_bug_history_table` (`id`, `user_id`, `bug_id`, `field_name`, `old_value`, `new_value`, `type`, `date_modified`) VALUES
	(56, 1, 15, '', '', '', 1, 1646664507),
	(93, 1, 24, '', '', '', 1, 1646746083),
	(94, 1, 25, '', '', '', 1, 1646746099),
	(95, 1, 26, '', '', '', 1, 1646746115),
	(108, 1, 27, '', '', '', 1, 1646759744),
	(109, 1, 24, 'view_state', '10', '50', 0, 1646759813),
	(110, 1, 28, '', '', '', 1, 1646759851),
	(113, 1, 24, 'handler_id', '0', '1', 0, 1646760118),
	(114, 1, 24, 'status', '10', '50', 0, 1646760118),
	(115, 1, 24, 'handler_id', '1', '0', 0, 1646760222),
	(116, 1, 25, '', '0000011', '', 2, 1646760308),
	(117, 1, 25, 'status', '10', '90', 0, 1646760322),
	(118, 1, 26, '', '0000012', '', 2, 1646760378),
	(119, 1, 25, '', '0000013', '', 2, 1646760438),
	(120, 1, 29, '', '', '', 1, 1646760475),
	(121, 1, 30, '', '', '', 1, 1646760498),
	(122, 1, 31, '', '', '', 1, 1646760520),
	(128, 1, 33, '', '', '', 1, 1646763631),
	(129, 1, 26, '', '0000014', '', 2, 1646763712),
	(130, 1, 25, '', '0000015', '', 2, 1646763726),
	(131, 1, 34, '', '', '', 1, 1646763769),
	(132, 1, 35, '', '', '', 1, 1646763787),
	(133, 1, 36, '', '', '', 1, 1646763806),
	(134, 1, 37, '', '', '', 1, 1646763824),
	(137, 1, 30, 'handler_id', '0', '13', 0, 1646763980),
	(138, 1, 30, 'status', '10', '50', 0, 1646763980),
	(139, 1, 25, '', '0000016', '', 2, 1646833766),
	(140, 1, 26, '', '0000017', '', 2, 1646833776),
	(141, 1, 38, '', '', '', 1, 1646833827),
	(142, 1, 39, '', '', '', 1, 1646833843),
	(143, 1, 40, '', '', '', 1, 1646833859),
	(144, 1, 41, '', '', '', 1, 1646833876),
	(148, 1, 39, 'handler_id', '0', '13', 0, 1646835774),
	(149, 1, 39, 'status', '10', '50', 0, 1646835774),
	(150, 1, 28, '', '0000018', '', 2, 1646856755),
	(151, 1, 28, '', '0000019', '', 2, 1646856766),
	(152, 1, 28, 'status', '10', '90', 0, 1646856777),
	(153, 1, 43, '', '', '', 1, 1646857220),
	(154, 1, 44, '', '', '', 1, 1646857237),
	(155, 1, 45, '', '', '', 1, 1646857253),
	(156, 1, 46, '', '', '', 1, 1646857269);
/*!40000 ALTER TABLE `mantis_bug_history_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bug_monitor_table
CREATE TABLE IF NOT EXISTS `mantis_bug_monitor_table` (
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `bug_id` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`,`bug_id`),
  KEY `idx_bug_id` (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bug_monitor_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_bug_monitor_table`;
/*!40000 ALTER TABLE `mantis_bug_monitor_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_bug_monitor_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bug_relationship_table
CREATE TABLE IF NOT EXISTS `mantis_bug_relationship_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_bug_id` int(10) unsigned NOT NULL DEFAULT 0,
  `destination_bug_id` int(10) unsigned NOT NULL DEFAULT 0,
  `relationship_type` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `idx_relationship_source` (`source_bug_id`),
  KEY `idx_relationship_destination` (`destination_bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bug_relationship_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_bug_relationship_table`;
/*!40000 ALTER TABLE `mantis_bug_relationship_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_bug_relationship_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bug_revision_table
CREATE TABLE IF NOT EXISTS `mantis_bug_revision_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bug_id` int(10) unsigned NOT NULL,
  `bugnote_id` int(10) unsigned NOT NULL DEFAULT 0,
  `user_id` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `value` longtext NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idx_bug_rev_type` (`type`),
  KEY `idx_bug_rev_id_time` (`bug_id`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bug_revision_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_bug_revision_table`;
/*!40000 ALTER TABLE `mantis_bug_revision_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_bug_revision_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bug_table
CREATE TABLE IF NOT EXISTS `mantis_bug_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT 0,
  `reporter_id` int(10) unsigned NOT NULL DEFAULT 0,
  `handler_id` int(10) unsigned NOT NULL DEFAULT 0,
  `duplicate_id` int(10) unsigned NOT NULL DEFAULT 0,
  `priority` smallint(6) NOT NULL DEFAULT 30,
  `severity` smallint(6) NOT NULL DEFAULT 50,
  `reproducibility` smallint(6) NOT NULL DEFAULT 10,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `resolution` smallint(6) NOT NULL DEFAULT 10,
  `projection` smallint(6) NOT NULL DEFAULT 10,
  `eta` smallint(6) NOT NULL DEFAULT 10,
  `bug_text_id` int(10) unsigned NOT NULL DEFAULT 0,
  `os` varchar(32) NOT NULL DEFAULT '',
  `os_build` varchar(32) NOT NULL DEFAULT '',
  `platform` varchar(32) NOT NULL DEFAULT '',
  `version` varchar(64) NOT NULL DEFAULT '',
  `fixed_in_version` varchar(64) NOT NULL DEFAULT '',
  `build` varchar(32) NOT NULL DEFAULT '',
  `profile_id` int(10) unsigned NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `summary` varchar(128) NOT NULL DEFAULT '',
  `sponsorship_total` int(11) NOT NULL DEFAULT 0,
  `sticky` tinyint(4) NOT NULL DEFAULT 0,
  `target_version` varchar(64) NOT NULL DEFAULT '',
  `category_id` int(10) unsigned NOT NULL DEFAULT 1,
  `date_submitted` int(10) unsigned NOT NULL DEFAULT 1,
  `due_date` int(10) unsigned NOT NULL DEFAULT 1,
  `last_updated` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idx_bug_sponsorship_total` (`sponsorship_total`),
  KEY `idx_bug_fixed_in_version` (`fixed_in_version`),
  KEY `idx_bug_status` (`status`),
  KEY `idx_project` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bug_table: ~22 rows (aproximadamente)
DELETE FROM `mantis_bug_table`;
/*!40000 ALTER TABLE `mantis_bug_table` DISABLE KEYS */;
INSERT INTO `mantis_bug_table` (`id`, `project_id`, `reporter_id`, `handler_id`, `duplicate_id`, `priority`, `severity`, `reproducibility`, `status`, `resolution`, `projection`, `eta`, `bug_text_id`, `os`, `os_build`, `platform`, `version`, `fixed_in_version`, `build`, `profile_id`, `view_state`, `summary`, `sponsorship_total`, `sticky`, `target_version`, `category_id`, `date_submitted`, `due_date`, `last_updated`) VALUES
	(15, 20, 1, 0, 0, 60, 70, 10, 10, 10, 10, 10, 15, '', '', '', '', '', '', 0, 10, 'Teste Resuasasmo3', 0, 0, '', 1, 1646664507, 1, 1646664507),
	(24, 20, 1, 0, 0, 30, 70, 50, 50, 10, 10, 10, 24, '', '', '', '', '', '', 0, 50, 'Teste de Automação com Data Driven Caso 2', 0, 0, '', 1, 1646746083, 1, 1646760222),
	(25, 20, 1, 0, 0, 40, 20, 90, 90, 10, 10, 10, 25, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 3', 0, 0, '', 1, 1646746099, 1, 1646833905),
	(26, 20, 1, 0, 0, 40, 10, 100, 10, 10, 10, 10, 26, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 4', 0, 0, '', 1, 1646746115, 1, 1646833776),
	(27, 20, 1, 0, 0, 60, 70, 10, 10, 10, 10, 10, 27, '', '', '', '', '', '', 0, 10, 'Teste Resumo3', 0, 0, '', 1, 1646759744, 1, 1646759744),
	(28, 20, 1, 0, 0, 60, 70, 10, 90, 10, 10, 10, 28, '', '', '', '', '', '', 0, 10, 'Teste Resumo3', 0, 0, '', 1, 1646759851, 1, 1646856777),
	(29, 20, 1, 0, 0, 20, 30, 10, 10, 10, 10, 10, 29, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 1', 0, 0, '', 1, 1646760475, 1, 1646760475),
	(30, 20, 1, 13, 0, 30, 70, 50, 50, 10, 10, 10, 30, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 2', 0, 0, '', 1, 1646760498, 1, 1646763980),
	(31, 20, 1, 0, 0, 40, 20, 90, 10, 10, 10, 10, 31, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 3', 0, 0, '', 1, 1646760520, 1, 1646760520),
	(33, 20, 1, 0, 0, 60, 70, 10, 10, 10, 10, 10, 33, '', '', '', '', '', '', 0, 10, 'Teste Resumo3', 0, 0, '', 1, 1646763631, 1, 1646763631),
	(34, 20, 1, 0, 0, 20, 30, 10, 10, 10, 10, 10, 34, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 1', 0, 0, '', 1, 1646763769, 1, 1646763769),
	(35, 20, 1, 0, 0, 30, 70, 50, 10, 10, 10, 10, 35, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 2', 0, 0, '', 1, 1646763787, 1, 1646763787),
	(36, 20, 1, 0, 0, 40, 20, 90, 10, 10, 10, 10, 36, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 3', 0, 0, '', 1, 1646763806, 1, 1646763806),
	(37, 20, 1, 0, 0, 40, 10, 100, 10, 10, 10, 10, 37, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 4', 0, 0, '', 1, 1646763824, 1, 1646763824),
	(38, 20, 1, 0, 0, 20, 30, 10, 10, 10, 10, 10, 38, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 1', 0, 0, '', 1, 1646833827, 1, 1646833827),
	(39, 20, 1, 13, 0, 30, 70, 50, 50, 10, 10, 10, 39, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 2', 0, 0, '', 1, 1646833843, 1, 1646835774),
	(40, 20, 1, 0, 0, 40, 20, 90, 10, 10, 10, 10, 40, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 3', 0, 0, '', 1, 1646833859, 1, 1646833859),
	(41, 20, 1, 0, 0, 40, 10, 100, 10, 10, 10, 10, 41, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 4', 0, 0, '', 1, 1646833876, 1, 1646833876),
	(43, 20, 1, 0, 0, 20, 30, 10, 10, 10, 10, 10, 43, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 1', 0, 0, '', 1, 1646857220, 1, 1646857220),
	(44, 20, 1, 0, 0, 30, 70, 50, 10, 10, 10, 10, 44, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 2', 0, 0, '', 1, 1646857237, 1, 1646857237),
	(45, 20, 1, 0, 0, 40, 20, 90, 10, 10, 10, 10, 45, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 3', 0, 0, '', 1, 1646857253, 1, 1646857253),
	(46, 20, 1, 0, 0, 40, 10, 100, 10, 10, 10, 10, 46, '', '', '', '', '', '', 0, 10, 'Teste de Automação com Data Driven Caso 4', 0, 0, '', 1, 1646857269, 1, 1646857269);
/*!40000 ALTER TABLE `mantis_bug_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bug_tag_table
CREATE TABLE IF NOT EXISTS `mantis_bug_tag_table` (
  `bug_id` int(10) unsigned NOT NULL DEFAULT 0,
  `tag_id` int(10) unsigned NOT NULL DEFAULT 0,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `date_attached` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`bug_id`,`tag_id`),
  KEY `idx_bug_tag_tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bug_tag_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_bug_tag_table`;
/*!40000 ALTER TABLE `mantis_bug_tag_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_bug_tag_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_bug_text_table
CREATE TABLE IF NOT EXISTS `mantis_bug_text_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` longtext NOT NULL,
  `steps_to_reproduce` longtext NOT NULL,
  `additional_information` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_bug_text_table: ~22 rows (aproximadamente)
DELETE FROM `mantis_bug_text_table`;
/*!40000 ALTER TABLE `mantis_bug_text_table` DISABLE KEYS */;
INSERT INTO `mantis_bug_text_table` (`id`, `description`, `steps_to_reproduce`, `additional_information`) VALUES
	(15, 'Teste Descrasasição3', 'asasa', ''),
	(24, 'Teste de Automação Base2', '', ''),
	(25, 'Teste de Automação Base2', '', ''),
	(26, 'Teste de Automação Base2', '', ''),
	(27, 'Teste Descrição3', '', ''),
	(28, 'Teste Descrição3', '', ''),
	(29, 'Teste de Automação Base2', '', ''),
	(30, 'Teste de Automação Base2', '', ''),
	(31, 'Teste de Automação Base2', '', ''),
	(33, 'Teste Descrição3', '', ''),
	(34, 'Teste de Automação Base2', '', ''),
	(35, 'Teste de Automação Base2', '', ''),
	(36, 'Teste de Automação Base2', '', ''),
	(37, 'Teste de Automação Base2', '', ''),
	(38, 'Teste de Automação Base2', '', ''),
	(39, 'Teste de Automação Base2', '', ''),
	(40, 'Teste de Automação Base2', '', ''),
	(41, 'Teste de Automação Base2', '', ''),
	(43, 'Teste de Automação Base2', '', ''),
	(44, 'Teste de Automação Base2', '', ''),
	(45, 'Teste de Automação Base2', '', ''),
	(46, 'Teste de Automação Base2', '', '');
/*!40000 ALTER TABLE `mantis_bug_text_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_category_table
CREATE TABLE IF NOT EXISTS `mantis_category_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT 0,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL DEFAULT '',
  `status` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_category_project_name` (`project_id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_category_table: ~2 rows (aproximadamente)
DELETE FROM `mantis_category_table`;
/*!40000 ALTER TABLE `mantis_category_table` DISABLE KEYS */;
INSERT INTO `mantis_category_table` (`id`, `project_id`, `user_id`, `name`, `status`) VALUES
	(1, 0, 0, 'General', 0),
	(13, 0, 1, 'Teste Formatado', 0);
/*!40000 ALTER TABLE `mantis_category_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_config_table
CREATE TABLE IF NOT EXISTS `mantis_config_table` (
  `config_id` varchar(64) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `access_reqd` int(11) DEFAULT 0,
  `type` int(11) DEFAULT 90,
  `value` longtext NOT NULL,
  PRIMARY KEY (`config_id`,`project_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_config_table: ~2 rows (aproximadamente)
DELETE FROM `mantis_config_table`;
/*!40000 ALTER TABLE `mantis_config_table` DISABLE KEYS */;
INSERT INTO `mantis_config_table` (`config_id`, `project_id`, `user_id`, `access_reqd`, `type`, `value`) VALUES
	('database_version', 0, 0, 90, 1, '209'),
	('plugin_MantisGraph_schema', 0, 0, 90, 1, '-1');
/*!40000 ALTER TABLE `mantis_config_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_custom_field_project_table
CREATE TABLE IF NOT EXISTS `mantis_custom_field_project_table` (
  `field_id` int(11) NOT NULL DEFAULT 0,
  `project_id` int(10) unsigned NOT NULL DEFAULT 0,
  `sequence` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`field_id`,`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_custom_field_project_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_custom_field_project_table`;
/*!40000 ALTER TABLE `mantis_custom_field_project_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_custom_field_project_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_custom_field_string_table
CREATE TABLE IF NOT EXISTS `mantis_custom_field_string_table` (
  `field_id` int(11) NOT NULL DEFAULT 0,
  `bug_id` int(11) NOT NULL DEFAULT 0,
  `value` varchar(255) NOT NULL DEFAULT '',
  `text` longtext DEFAULT NULL,
  PRIMARY KEY (`field_id`,`bug_id`),
  KEY `idx_custom_field_bug` (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_custom_field_string_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_custom_field_string_table`;
/*!40000 ALTER TABLE `mantis_custom_field_string_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_custom_field_string_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_custom_field_table
CREATE TABLE IF NOT EXISTS `mantis_custom_field_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `type` smallint(6) NOT NULL DEFAULT 0,
  `possible_values` text DEFAULT NULL,
  `default_value` varchar(255) NOT NULL DEFAULT '',
  `valid_regexp` varchar(255) NOT NULL DEFAULT '',
  `access_level_r` smallint(6) NOT NULL DEFAULT 0,
  `access_level_rw` smallint(6) NOT NULL DEFAULT 0,
  `length_min` int(11) NOT NULL DEFAULT 0,
  `length_max` int(11) NOT NULL DEFAULT 0,
  `require_report` tinyint(4) NOT NULL DEFAULT 0,
  `require_update` tinyint(4) NOT NULL DEFAULT 0,
  `display_report` tinyint(4) NOT NULL DEFAULT 0,
  `display_update` tinyint(4) NOT NULL DEFAULT 1,
  `require_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `display_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `display_closed` tinyint(4) NOT NULL DEFAULT 0,
  `require_closed` tinyint(4) NOT NULL DEFAULT 0,
  `filter_by` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idx_custom_field_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_custom_field_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_custom_field_table`;
/*!40000 ALTER TABLE `mantis_custom_field_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_custom_field_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_email_table
CREATE TABLE IF NOT EXISTS `mantis_email_table` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(64) NOT NULL DEFAULT '',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `metadata` longtext NOT NULL,
  `body` longtext NOT NULL,
  `submitted` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_email_table: ~30 rows (aproximadamente)
DELETE FROM `mantis_email_table`;
/*!40000 ALTER TABLE `mantis_email_table` DISABLE KEYS */;
INSERT INTO `mantis_email_table` (`email_id`, `email`, `subject`, `metadata`, `body`, `submitted`) VALUES
	(23, 'dsdssd@gmail.com', '[MantisBT] Account registration', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The user administrator has created an account for you with username "dsasds". In order to complete your registration, visit the following URL (make sure it is entered as the single line) and set your own access password:\n\nhttp://127.0.0.1:8989/verify.php?id=15&confirm_hash=9HgMJCHuHKN4o5WvjyDw3LU--4l9-iLMRurIuW9zeUwdM0NTtp1NVk3iKg83xXG6b95NSmDr5IynaBPKP3pb\n\nIf you did not request any registration, ignore this message and nothing will happen.\n\nDo not reply to this message', 1646416701),
	(24, 'dsdssd@gmail.com', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nUsername dsasds => dsasdssa', 1646416731),
	(25, 'dadadada@gmail.com', '[MantisBT] Account registration', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The user administrator has created an account for you with username "saassa". In order to complete your registration, visit the following URL (make sure it is entered as the single line) and set your own access password:\n\nhttp://127.0.0.1:8989/verify.php?id=16&confirm_hash=PpDXk1LjHyeXZG0BOSWrfVuE1RVcIlchVG5-au7dUbhh6knNbSKZ6JXmF0bYwZSj_4v9zso4edpm3Abx7uFb\n\nIf you did not request any registration, ignore this message and nothing will happen.\n\nDo not reply to this message', 1646416755),
	(26, 'matheus.santos.pereira@base2.com.br', '[MantisBT] Account registration', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The user administrator has created an account for you with username "Matheus.Santos". In order to complete your registration, visit the following URL (make sure it is entered as the single line) and set your own access password:\n\nhttp://127.0.0.1:8989/verify.php?id=17&confirm_hash=DsBA1mJbPM_r-5AuJMwnLSRort5OlFpfRQ9G6w0AXnWxiPhaX5CkiBjjxQNvnv1atBhEwCTWij21BWAEYe0r\n\nIf you did not request any registration, ignore this message and nothing will happen.\n\nDo not reply to this message', 1646417023),
	(27, 'matheus.santos.pereira@base2.com.br', '[MantisBT] Account registration', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The user administrator has created an account for you with username "Matheus.Santos". In order to complete your registration, visit the following URL (make sure it is entered as the single line) and set your own access password:\n\nhttp://127.0.0.1:8989/verify.php?id=18&confirm_hash=b38EYAhCxpbiutU09Xl9ZHO0agzc0kVrVHeVzsvFNnitSjg5WOMZf-yXwH3cj1BvkRij7OdtUWfMPGdFkg0X\n\nIf you did not request any registration, ignore this message and nothing will happen.\n\nDo not reply to this message', 1646417099),
	(28, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level manager => updater', 1646658117),
	(29, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level updater => manager', 1646658385),
	(30, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level manager => reporter', 1646658466),
	(31, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level reporter => manager', 1646671208),
	(32, 'lucas.cunha@base2.com.br', '[Test 0000011]: Teste de Automação com Data Driven Caso 1', 'a:3:{s:7:"headers";a:2:{s:8:"keywords";s:14:"[Test] General";s:11:"In-Reply-To";s:32:"86271ec4956e01992fde77696d6c9949";}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The following issue has been ASSIGNED. \n====================================================================== \nhttp://127.0.0.1:8989/view.php?id=11 \n====================================================================== \nReported By:                administrator\nAssigned To:                Lucas.Cunha\n====================================================================== \nProject:                    Test\nIssue ID:                   11\nCategory:                   General\nReproducibility:            always\nSeverity:                   text\nPriority:                   low\nStatus:                     assigned\n====================================================================== \nDate Submitted:             2022-02-28 15:08 BRT\nLast Modified:              2022-03-07 13:40 BRT\n====================================================================== \nSummary:                    Teste de Automação com Data Driven Caso 1\nDescription: \nTeste de Automação Base2\n====================================================================== \n\nIssue History \nDate Modified    Username       Field                    Change               \n====================================================================== \n2022-02-28 15:08 administrator  New Issue                                    \n2022-03-07 13:40 administrator  Assigned To               => Lucas.Cunha     \n2022-03-07 13:40 administrator  Status                   new => assigned     \n======================================================================', 1646671233),
	(33, 'lucas.cunha@base2.com.br', '[Test 0000014]: Teste de Automação com Data Driven Caso 4', 'a:3:{s:7:"headers";a:2:{s:8:"keywords";s:14:"[Test] General";s:11:"In-Reply-To";s:32:"551bfdf4f733148db9ce58494f69ce21";}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The following issue has been ASSIGNED. \n====================================================================== \nhttp://127.0.0.1:8989/view.php?id=14 \n====================================================================== \nReported By:                administrator\nAssigned To:                Lucas.Cunha\n====================================================================== \nProject:                    Test\nIssue ID:                   14\nCategory:                   General\nReproducibility:            N/A\nSeverity:                   feature\nPriority:                   high\nStatus:                     assigned\n====================================================================== \nDate Submitted:             2022-02-28 15:09 BRT\nLast Modified:              2022-03-07 13:41 BRT\n====================================================================== \nSummary:                    Teste de Automação com Data Driven Caso 4\nDescription: \nTeste de Automação Base2\n====================================================================== \n\nIssue History \nDate Modified    Username       Field                    Change               \n====================================================================== \n2022-02-28 15:09 administrator  New Issue                                    \n2022-02-28 19:14 administrator  Issue Monitored: administrator                    \n2022-02-28 19:14 administrator  Issue End Monitor: administrator                    \n2022-03-07 13:41 administrator  Assigned To               => Lucas.Cunha     \n2022-03-07 13:41 administrator  Status                   new => assigned     \n======================================================================', 1646671272),
	(34, 'lucas.cunha@base2.com.br', '[Test 0000014]: Teste de Automação com Data Driven Caso 4', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000008) administrator (administrator) - 2022-03-08 07:49\n http://127.0.0.1:8989/view.php?id=14#c8 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n.\n----------------------------------------------------------------------', 1646736573),
	(35, 'lucas.cunha@base2.com.br', '[Test 0000014]: Teste de Automação com Data Driven Caso 4', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000009) administrator (administrator) - 2022-03-08 07:51\n http://127.0.0.1:8989/view.php?id=14#c9 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n.\n----------------------------------------------------------------------', 1646736692),
	(36, 'lucas.cunha@base2.com.br', '[Test 0000014]: Teste de Automação com Data Driven Caso 4', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000010) administrator (administrator) - 2022-03-08 07:55 (private)\n http://127.0.0.1:8989/view.php?id=14#c10 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n.\n----------------------------------------------------------------------', 1646736909),
	(37, 'lucas.cunha@base2.com.br', '[Test 0000014]: Teste de Automação com Data Driven Caso 4', 'a:3:{s:7:"headers";a:2:{s:8:"keywords";s:14:"[Test] General";s:11:"In-Reply-To";s:32:"551bfdf4f733148db9ce58494f69ce21";}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The following issue has been CLOSED \n====================================================================== \nhttp://127.0.0.1:8989/view.php?id=14 \n====================================================================== \nReported By:                administrator\nAssigned To:                Lucas.Cunha\n====================================================================== \nProject:                    Test\nIssue ID:                   14\nCategory:                   General\nReproducibility:            N/A\nSeverity:                   feature\nPriority:                   high\nStatus:                     closed\nResolution:                 open\nFixed in Version:           \n====================================================================== \nDate Submitted:             2022-02-28 15:09 BRT\nLast Modified:              2022-03-08 08:08 BRT\n====================================================================== \nSummary:                    Teste de Automação com Data Driven Caso 4\nDescription: \nTeste de Automação Base2\n====================================================================== \n\n---------------------------------------------------------------------- \n (0000009) administrator (administrator) - 2022-03-08 07:51\n http://127.0.0.1:8989/view.php?id=14#c9 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n. \n\n---------------------------------------------------------------------- \n (0000010) administrator (administrator) - 2022-03-08 07:55 (private)\n http://127.0.0.1:8989/view.php?id=14#c10 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n. \n\nIssue History \nDate Modified    Username       Field                    Change               \n====================================================================== \n2022-02-28 15:09 administrator  New Issue                                    \n2022-02-28 19:14 administrator  Issue Monitored: administrator                    \n2022-02-28 19:14 administrator  Issue End Monitor: administrator                    \n2022-03-07 13:41 administrator  Assigned To               => Lucas.Cunha     \n2022-03-07 13:41 administrator  Status                   new => assigned     \n2022-03-08 07:51 administrator  Note Added: 0000009                          \n2022-03-08 07:55 administrator  Note Added: 0000010                          \n2022-03-08 08:08 administrator  Status                   assigned => closed  \n======================================================================', 1646737695),
	(38, 'lucas.cunha@base2.com.br', '[Test 0000014]: Teste de Automação com Data Driven Caso 4', 'a:3:{s:7:"headers";a:2:{s:8:"keywords";s:14:"[Test] General";s:11:"In-Reply-To";s:32:"551bfdf4f733148db9ce58494f69ce21";}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The following issue has been REOPENED. \n====================================================================== \nhttp://127.0.0.1:8989/view.php?id=14 \n====================================================================== \nReported By:                administrator\nAssigned To:                Lucas.Cunha\n====================================================================== \nProject:                    Test\nIssue ID:                   14\nCategory:                   General\nReproducibility:            N/A\nSeverity:                   feature\nPriority:                   high\nStatus:                     new\n====================================================================== \nDate Submitted:             2022-02-28 15:09 BRT\nLast Modified:              2022-03-08 08:09 BRT\n====================================================================== \nSummary:                    Teste de Automação com Data Driven Caso 4\nDescription: \nTeste de Automação Base2\n====================================================================== \n\n---------------------------------------------------------------------- \n (0000009) administrator (administrator) - 2022-03-08 07:51\n http://127.0.0.1:8989/view.php?id=14#c9 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n. \n\n---------------------------------------------------------------------- \n (0000010) administrator (administrator) - 2022-03-08 07:55 (private)\n http://127.0.0.1:8989/view.php?id=14#c10 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n. \n\nIssue History \nDate Modified    Username       Field                    Change               \n====================================================================== \n2022-02-28 15:09 administrator  New Issue                                    \n2022-02-28 19:14 administrator  Issue Monitored: administrator                    \n2022-02-28 19:14 administrator  Issue End Monitor: administrator                    \n2022-03-07 13:41 administrator  Assigned To               => Lucas.Cunha     \n2022-03-07 13:41 administrator  Status                   new => assigned     \n2022-03-08 07:51 administrator  Note Added: 0000009                          \n2022-03-08 07:55 administrator  Note Added: 0000010                          \n2022-03-08 08:08 administrator  Status                   assigned => closed  \n2022-03-08 08:09 administrator  Status                   closed => new       \n2022-03-08 08:09 administrator  Resolution               open => reopened    \n======================================================================', 1646737772),
	(39, 'lucas.cunha@base2.com.br', '[Test 0000014]: Teste de Automação com Data Driven Caso 4', 'a:3:{s:7:"headers";a:2:{s:8:"keywords";s:14:"[Test] General";s:11:"In-Reply-To";s:32:"551bfdf4f733148db9ce58494f69ce21";}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The following issue has been CLOSED \n====================================================================== \nhttp://127.0.0.1:8989/view.php?id=14 \n====================================================================== \nReported By:                administrator\nAssigned To:                Lucas.Cunha\n====================================================================== \nProject:                    Test\nIssue ID:                   14\nCategory:                   General\nReproducibility:            N/A\nSeverity:                   feature\nPriority:                   high\nStatus:                     closed\nResolution:                 reopened\nFixed in Version:           \n====================================================================== \nDate Submitted:             2022-02-28 15:09 BRT\nLast Modified:              2022-03-08 08:10 BRT\n====================================================================== \nSummary:                    Teste de Automação com Data Driven Caso 4\nDescription: \nTeste de Automação Base2\n====================================================================== \n\n---------------------------------------------------------------------- \n (0000009) administrator (administrator) - 2022-03-08 07:51\n http://127.0.0.1:8989/view.php?id=14#c9 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n. \n\n---------------------------------------------------------------------- \n (0000010) administrator (administrator) - 2022-03-08 07:55 (private)\n http://127.0.0.1:8989/view.php?id=14#c10 \n---------------------------------------------------------------------- \nRealizar uma anotação pública e realizar a asserção pela n. \n\nIssue History \nDate Modified    Username       Field                    Change               \n====================================================================== \n2022-02-28 15:09 administrator  New Issue                                    \n2022-02-28 19:14 administrator  Issue Monitored: administrator                    \n2022-02-28 19:14 administrator  Issue End Monitor: administrator                    \n2022-03-07 13:41 administrator  Assigned To               => Lucas.Cunha     \n2022-03-07 13:41 administrator  Status                   new => assigned     \n2022-03-08 07:51 administrator  Note Added: 0000009                          \n2022-03-08 07:55 administrator  Note Added: 0000010                          \n2022-03-08 08:08 administrator  Status                   assigned => closed  \n2022-03-08 08:09 administrator  Status                   closed => new       \n2022-03-08 08:09 administrator  Resolution               open => reopened    \n2022-03-08 08:10 administrator  Status                   new => closed       \n======================================================================', 1646737848),
	(40, 'matheus.santos.pereira@base2.com.br', '[MantisBT] Account registration', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The user administrator has created an account for you with username "Matheus.Santos". In order to complete your registration, visit the following URL (make sure it is entered as the single line) and set your own access password:\n\nhttp://127.0.0.1:8989/verify.php?id=19&confirm_hash=4nP0xtr0GNEj0i-DM-hUsxy1lZoyIIT00FmvfUETfLIA344kdf5EG6C54NT6xl5K7xv1qRj_9EY_pi3f7O3s\n\nIf you did not request any registration, ignore this message and nothing will happen.\n\nDo not reply to this message', 1646758731),
	(41, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level manager => reporter', 1646759393),
	(42, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level reporter => manager', 1646759952),
	(43, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level manager => viewer', 1646760716),
	(44, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level viewer => updater', 1646760824),
	(45, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level updater => manager', 1646760973),
	(46, 'lucas.cunha@base2.com.br', '[hello 0000032]: Teste de Automação com Data Driven Caso 4', 'a:3:{s:7:"headers";a:2:{s:8:"keywords";s:15:"[hello] General";s:11:"In-Reply-To";s:32:"69c9a3aae20ade83898d6561ebd56652";}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The following issue has been ASSIGNED. \n====================================================================== \nhttp://127.0.0.1:8989/view.php?id=32 \n====================================================================== \nReported By:                administrator\nAssigned To:                Lucas.Cunha\n====================================================================== \nProject:                    hello\nIssue ID:                   32\nCategory:                   General\nReproducibility:            N/A\nSeverity:                   feature\nPriority:                   high\nStatus:                     assigned\n====================================================================== \nDate Submitted:             2022-03-08 14:29 BRT\nLast Modified:              2022-03-08 14:38 BRT\n====================================================================== \nSummary:                    Teste de Automação com Data Driven Caso 4\nDescription: \nTeste de Automação Base2\n====================================================================== \n\nIssue History \nDate Modified    Username       Field                    Change               \n====================================================================== \n2022-03-08 14:29 administrator  New Issue                                    \n2022-03-08 14:35 administrator  Assigned To               => administrator   \n2022-03-08 14:35 administrator  Status                   new => assigned     \n2022-03-08 14:38 administrator  Assigned To              administrator =>    \n2022-03-08 14:38 administrator  Assigned To               => Lucas.Cunha     \n======================================================================', 1646761105),
	(47, 'matheus.santos.pereira@base2.com.br', '[MantisBT] Account registration', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The user administrator has created an account for you with username "Matheus.Santos". In order to complete your registration, visit the following URL (make sure it is entered as the single line) and set your own access password:\n\nhttp://127.0.0.1:8989/verify.php?id=20&confirm_hash=OwkYRJ4u5b4jKIBAu1AQRViHOxMAnB1pRY9UGQVwuO9v8BJe0tXpwe1gLAB96D5w51lSwWND4fxQncCY7GwT\n\nIf you did not request any registration, ignore this message and nothing will happen.\n\nDo not reply to this message', 1646763031),
	(48, 'lucas.cunha@base2.com.br', '[MantisBT] Account updated', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'Your account has been updated by an administrator. A list of these changes is provided below. You can update your account details and preferences at any time by visiting the following URL:\n\nhttp://127.0.0.1:8989/account_page.php\n\nAccess Level manager => developer', 1646763105),
	(49, 'lucas.cunha@base2.com.br', '[hello 0000030]: Teste de Automação com Data Driven Caso 2', 'a:3:{s:7:"headers";a:2:{s:8:"keywords";s:15:"[hello] General";s:11:"In-Reply-To";s:32:"579ccd2087ba4afae08a1e9a2e61a7dd";}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The following issue has been ASSIGNED. \n====================================================================== \nhttp://127.0.0.1:8989/view.php?id=30 \n====================================================================== \nReported By:                administrator\nAssigned To:                Lucas.Cunha\n====================================================================== \nProject:                    hello\nIssue ID:                   30\nCategory:                   General\nReproducibility:            random\nSeverity:                   crash\nPriority:                   normal\nStatus:                     assigned\n====================================================================== \nDate Submitted:             2022-03-08 14:28 BRT\nLast Modified:              2022-03-08 15:26 BRT\n====================================================================== \nSummary:                    Teste de Automação com Data Driven Caso 2\nDescription: \nTeste de Automação Base2\n====================================================================== \n\nIssue History \nDate Modified    Username       Field                    Change               \n====================================================================== \n2022-03-08 14:28 administrator  New Issue                                    \n2022-03-08 15:26 administrator  Assigned To               => Lucas.Cunha     \n2022-03-08 15:26 administrator  Status                   new => assigned     \n======================================================================', 1646763980),
	(50, 'matheus.santos.pereira@base2.com.br', '[MantisBT] Account registration', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The user administrator has created an account for you with username "Matheus.Santos". In order to complete your registration, visit the following URL (make sure it is entered as the single line) and set your own access password:\n\nhttp://127.0.0.1:8989/verify.php?id=21&confirm_hash=mEoeJw4Buc4aTsr_Y2l4veCnPi5Z5hCbV8DN0Guy2c6Xb2Y5vesr3HWV9zLND33NJO6VcUil7h3pc6JKtflk\n\nIf you did not request any registration, ignore this message and nothing will happen.\n\nDo not reply to this message', 1646833592),
	(51, 'lucas.cunha@base2.com.br', '[hello 0000039]: Teste de Automação com Data Driven Caso 2', 'a:3:{s:7:"headers";a:2:{s:8:"keywords";s:15:"[hello] General";s:11:"In-Reply-To";s:32:"a3f2c64c21a041db0ba15ea655c5d728";}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The following issue has been ASSIGNED. \n====================================================================== \nhttp://127.0.0.1:8989/view.php?id=39 \n====================================================================== \nReported By:                administrator\nAssigned To:                Lucas.Cunha\n====================================================================== \nProject:                    hello\nIssue ID:                   39\nCategory:                   General\nReproducibility:            random\nSeverity:                   crash\nPriority:                   normal\nStatus:                     assigned\n====================================================================== \nDate Submitted:             2022-03-09 10:50 BRT\nLast Modified:              2022-03-09 11:22 BRT\n====================================================================== \nSummary:                    Teste de Automação com Data Driven Caso 2\nDescription: \nTeste de Automação Base2\n====================================================================== \n\nIssue History \nDate Modified    Username       Field                    Change               \n====================================================================== \n2022-03-09 10:50 administrator  New Issue                                    \n2022-03-09 11:22 administrator  Assigned To               => Lucas.Cunha     \n2022-03-09 11:22 administrator  Status                   new => assigned     \n======================================================================', 1646835774),
	(52, 'matheus.santos.pereira@base2.com.br', '[MantisBT] Account registration', 'a:3:{s:7:"headers";a:0:{}s:7:"charset";s:5:"utf-8";s:8:"hostname";s:9:"127.0.0.1";}', 'The user administrator has created an account for you with username "Matheus.Santos". In order to complete your registration, visit the following URL (make sure it is entered as the single line) and set your own access password:\n\nhttp://127.0.0.1:8989/verify.php?id=22&confirm_hash=j6UwiVZGyxQbS9y9QfX_ajPNDq_Hi-WqR2RVkCvKe4Z2jk4D80yQ8A_bZ6MkgSSpurrt1VPfCy2hsi3ZB16U\n\nIf you did not request any registration, ignore this message and nothing will happen.\n\nDo not reply to this message', 1646857542);
/*!40000 ALTER TABLE `mantis_email_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_filters_table
CREATE TABLE IF NOT EXISTS `mantis_filters_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `is_public` tinyint(4) DEFAULT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `filter_string` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_filters_table: ~3 rows (aproximadamente)
DELETE FROM `mantis_filters_table`;
/*!40000 ALTER TABLE `mantis_filters_table` DISABLE KEYS */;
INSERT INTO `mantis_filters_table` (`id`, `user_id`, `project_id`, `is_public`, `name`, `filter_string`) VALUES
	(1, 1, 0, 0, '', 'v9#{"_version":"v9","_view_type":"simple","category_id":["0"],"severity":[0],"status":[0],"per_page":50,"highlight_changed":6,"reporter_id":[0],"handler_id":[0],"project_id":[-3],"sort":"last_updated","dir":"DESC","filter_by_date":false,"start_month":3,"start_day":1,"start_year":2022,"end_month":3,"end_day":8,"end_year":2022,"filter_by_last_updated_date":false,"last_updated_start_month":3,"last_updated_start_day":1,"last_updated_start_year":2022,"last_updated_end_month":3,"last_updated_end_day":8,"last_updated_end_year":2022,"search":"","hide_status":[-2],"resolution":[0],"build":["0"],"version":["0"],"fixed_in_version":["0"],"target_version":["0"],"priority":[0],"monitor_user_id":[0],"view_state":0,"custom_fields":[],"sticky":true,"relationship_type":-1,"relationship_bug":0,"profile_id":[0],"platform":["0"],"os":["0"],"os_build":["0"],"tag_string":"","tag_select":0,"note_user_id":[0],"match_type":0}'),
	(2, 1, -15, 0, '', 'v9#{"_version":"v9","_view_type":"simple","category_id":["0"],"severity":[0],"status":[0],"per_page":50,"highlight_changed":6,"reporter_id":[0],"handler_id":[0],"project_id":[-3],"sort":"last_updated","dir":"DESC","filter_by_date":false,"start_month":2,"start_day":1,"start_year":2022,"end_month":2,"end_day":25,"end_year":2022,"filter_by_last_updated_date":false,"last_updated_start_month":2,"last_updated_start_day":1,"last_updated_start_year":2022,"last_updated_end_month":2,"last_updated_end_day":25,"last_updated_end_year":2022,"search":"","hide_status":[90],"resolution":[0],"build":["0"],"version":["0"],"fixed_in_version":["0"],"target_version":["0"],"priority":[0],"monitor_user_id":[0],"view_state":0,"custom_fields":[],"sticky":true,"relationship_type":-1,"relationship_bug":0,"profile_id":[0],"platform":["0"],"os":["0"],"os_build":["0"],"tag_string":"","tag_select":0,"note_user_id":[0],"match_type":0,"_source_query_id":2}'),
	(3, 1, -16, 0, '', 'v9#{"_version":"v9","_view_type":"simple","category_id":["0"],"severity":[0],"status":[0],"highlight_changed":6,"reporter_id":[0],"handler_id":[0],"project_id":[-3],"resolution":[0],"build":["0"],"version":["0"],"hide_status":[90],"monitor_user_id":[0],"sort":"last_updated","dir":"DESC","per_page":50,"match_type":0,"platform":["0"],"os":["0"],"os_build":["0"],"fixed_in_version":["0"],"target_version":["0"],"profile_id":[0],"priority":[0],"note_user_id":[0],"sticky":true,"filter_by_date":false,"start_month":"02","end_month":"02","start_day":1,"end_day":"25","start_year":"2022","end_year":"2022","filter_by_last_updated_date":false,"last_updated_start_month":"02","last_updated_end_month":"02","last_updated_start_day":1,"last_updated_end_day":"25","last_updated_start_year":"2022","last_updated_end_year":"2022","search":"","view_state":0,"tag_string":"","tag_select":0,"relationship_type":-1,"relationship_bug":0,"custom_fields":[],"_source_query_id":3}');
/*!40000 ALTER TABLE `mantis_filters_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_news_table
CREATE TABLE IF NOT EXISTS `mantis_news_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT 0,
  `poster_id` int(10) unsigned NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `announcement` tinyint(4) NOT NULL DEFAULT 0,
  `headline` varchar(64) NOT NULL DEFAULT '',
  `body` longtext NOT NULL,
  `last_modified` int(10) unsigned NOT NULL DEFAULT 1,
  `date_posted` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_news_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_news_table`;
/*!40000 ALTER TABLE `mantis_news_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_news_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_plugin_table
CREATE TABLE IF NOT EXISTS `mantis_plugin_table` (
  `basename` varchar(40) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 0,
  `protected` tinyint(4) NOT NULL DEFAULT 0,
  `priority` int(10) unsigned NOT NULL DEFAULT 3,
  PRIMARY KEY (`basename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_plugin_table: ~2 rows (aproximadamente)
DELETE FROM `mantis_plugin_table`;
/*!40000 ALTER TABLE `mantis_plugin_table` DISABLE KEYS */;
INSERT INTO `mantis_plugin_table` (`basename`, `enabled`, `protected`, `priority`) VALUES
	('MantisCoreFormatting', 1, 0, 3);
/*!40000 ALTER TABLE `mantis_plugin_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_project_file_table
CREATE TABLE IF NOT EXISTS `mantis_project_file_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT 0,
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `diskfile` varchar(250) NOT NULL DEFAULT '',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `folder` varchar(250) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT 0,
  `file_type` varchar(250) NOT NULL DEFAULT '',
  `content` longblob DEFAULT NULL,
  `date_added` int(10) unsigned NOT NULL DEFAULT 1,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_project_file_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_project_file_table`;
/*!40000 ALTER TABLE `mantis_project_file_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_project_file_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_project_hierarchy_table
CREATE TABLE IF NOT EXISTS `mantis_project_hierarchy_table` (
  `child_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `inherit_parent` tinyint(4) NOT NULL DEFAULT 0,
  UNIQUE KEY `idx_project_hierarchy` (`child_id`,`parent_id`),
  KEY `idx_project_hierarchy_child_id` (`child_id`),
  KEY `idx_project_hierarchy_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_project_hierarchy_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_project_hierarchy_table`;
/*!40000 ALTER TABLE `mantis_project_hierarchy_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_project_hierarchy_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_project_table
CREATE TABLE IF NOT EXISTS `mantis_project_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `status` smallint(6) NOT NULL DEFAULT 10,
  `enabled` tinyint(4) NOT NULL DEFAULT 1,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `access_min` smallint(6) NOT NULL DEFAULT 10,
  `file_path` varchar(250) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT 1,
  `inherit_global` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_project_name` (`name`),
  KEY `idx_project_view` (`view_state`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_project_table: ~6 rows (aproximadamente)
DELETE FROM `mantis_project_table`;
/*!40000 ALTER TABLE `mantis_project_table` DISABLE KEYS */;
INSERT INTO `mantis_project_table` (`id`, `name`, `status`, `enabled`, `view_state`, `access_min`, `file_path`, `description`, `category_id`, `inherit_global`) VALUES
	(20, 'hello', 10, 1, 50, 10, '', 'hello', 1, 1),
	(26, 'Teste Formatado', 30, 1, 10, 10, '', 'Descrição do Projeto Utilizando Template Base2', 1, 1),
	(39, 'Desenvolvimento Inicial', 30, 1, 10, 10, '', 'Desenvolvimento de CT com Data Driven 1', 1, 1),
	(40, 'Desenvolvimento Medio', 50, 1, 10, 10, '', 'Desenvolvimento de CT com Data Driven 2', 1, 1),
	(41, 'Desenvolvimento Final', 10, 1, 10, 10, '', 'Desenvolvimento de CT com Data Driven 3', 1, 1),
	(42, 'Teste Formatado Reteste', 30, 1, 10, 10, '', 'Descrição do Projeto Utilizando Template Base2', 1, 1);
/*!40000 ALTER TABLE `mantis_project_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_project_user_list_table
CREATE TABLE IF NOT EXISTS `mantis_project_user_list_table` (
  `project_id` int(10) unsigned NOT NULL DEFAULT 0,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `access_level` smallint(6) NOT NULL DEFAULT 10,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `idx_project_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_project_user_list_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_project_user_list_table`;
/*!40000 ALTER TABLE `mantis_project_user_list_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_project_user_list_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_project_version_table
CREATE TABLE IF NOT EXISTS `mantis_project_version_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL DEFAULT 0,
  `version` varchar(64) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `released` tinyint(4) NOT NULL DEFAULT 1,
  `obsolete` tinyint(4) NOT NULL DEFAULT 0,
  `date_order` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_project_version` (`project_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_project_version_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_project_version_table`;
/*!40000 ALTER TABLE `mantis_project_version_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_project_version_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_sponsorship_table
CREATE TABLE IF NOT EXISTS `mantis_sponsorship_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bug_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `amount` int(11) NOT NULL DEFAULT 0,
  `logo` varchar(128) NOT NULL DEFAULT '',
  `url` varchar(128) NOT NULL DEFAULT '',
  `paid` tinyint(4) NOT NULL DEFAULT 0,
  `date_submitted` int(10) unsigned NOT NULL DEFAULT 1,
  `last_updated` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idx_sponsorship_bug_id` (`bug_id`),
  KEY `idx_sponsorship_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_sponsorship_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_sponsorship_table`;
/*!40000 ALTER TABLE `mantis_sponsorship_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_sponsorship_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_tag_table
CREATE TABLE IF NOT EXISTS `mantis_tag_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `date_created` int(10) unsigned NOT NULL DEFAULT 1,
  `date_updated` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`,`name`),
  KEY `idx_tag_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_tag_table: ~2 rows (aproximadamente)
DELETE FROM `mantis_tag_table`;
/*!40000 ALTER TABLE `mantis_tag_table` DISABLE KEYS */;
INSERT INTO `mantis_tag_table` (`id`, `user_id`, `name`, `description`, `date_created`, `date_updated`) VALUES
	(3, 1, 'Testeee', 'testes', 1646218466, 1646218466),
	(12, 1, 'Marcador Formatado', 'Descrição do Marcador pare execução de Testes no Template Base2', 1646857443, 1646857443);
/*!40000 ALTER TABLE `mantis_tag_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_tokens_table
CREATE TABLE IF NOT EXISTS `mantis_tokens_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `value` longtext NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 1,
  `expiry` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idx_typeowner` (`type`,`owner`)
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_tokens_table: ~2 rows (aproximadamente)
DELETE FROM `mantis_tokens_table`;
/*!40000 ALTER TABLE `mantis_tokens_table` DISABLE KEYS */;
INSERT INTO `mantis_tokens_table` (`id`, `owner`, `type`, `value`, `timestamp`, `expiry`) VALUES
	(8, 1, 5, '{"filter":false,"sidebar":false,"profile":false,"timeline":false}', 1645624354, 1677840570),
	(281, 1, 4, '1', 1647870781, 1647871085);
/*!40000 ALTER TABLE `mantis_tokens_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_user_pref_table
CREATE TABLE IF NOT EXISTS `mantis_user_pref_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `project_id` int(10) unsigned NOT NULL DEFAULT 0,
  `default_profile` int(10) unsigned NOT NULL DEFAULT 0,
  `default_project` int(10) unsigned NOT NULL DEFAULT 0,
  `refresh_delay` int(11) NOT NULL DEFAULT 0,
  `redirect_delay` int(11) NOT NULL DEFAULT 0,
  `bugnote_order` varchar(4) NOT NULL DEFAULT 'ASC',
  `email_on_new` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_assigned` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_feedback` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_closed` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_reopened` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_bugnote` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_status` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_priority` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_priority_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_status_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_bugnote_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_reopened_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_closed_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_resolved_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_feedback_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_assigned_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_new_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_bugnote_limit` smallint(6) NOT NULL DEFAULT 0,
  `language` varchar(32) NOT NULL DEFAULT 'english',
  `timezone` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_user_pref_table: ~2 rows (aproximadamente)
DELETE FROM `mantis_user_pref_table`;
/*!40000 ALTER TABLE `mantis_user_pref_table` DISABLE KEYS */;
INSERT INTO `mantis_user_pref_table` (`id`, `user_id`, `project_id`, `default_profile`, `default_project`, `refresh_delay`, `redirect_delay`, `bugnote_order`, `email_on_new`, `email_on_assigned`, `email_on_feedback`, `email_on_resolved`, `email_on_closed`, `email_on_reopened`, `email_on_bugnote`, `email_on_status`, `email_on_priority`, `email_on_priority_min_severity`, `email_on_status_min_severity`, `email_on_bugnote_min_severity`, `email_on_reopened_min_severity`, `email_on_closed_min_severity`, `email_on_resolved_min_severity`, `email_on_feedback_min_severity`, `email_on_assigned_min_severity`, `email_on_new_min_severity`, `email_bugnote_limit`, `language`, `timezone`) VALUES
	(2, 13, 0, 0, 0, 30, 2, 'ASC', 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'auto', 'Asia/Shanghai'),
	(3, 1, 0, 0, 0, 15, 2, 'ASC', 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'auto', 'America/Sao_Paulo');
/*!40000 ALTER TABLE `mantis_user_pref_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_user_print_pref_table
CREATE TABLE IF NOT EXISTS `mantis_user_print_pref_table` (
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `print_pref` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_user_print_pref_table: ~0 rows (aproximadamente)
DELETE FROM `mantis_user_print_pref_table`;
/*!40000 ALTER TABLE `mantis_user_print_pref_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantis_user_print_pref_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_user_profile_table
CREATE TABLE IF NOT EXISTS `mantis_user_profile_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `platform` varchar(32) NOT NULL DEFAULT '',
  `os` varchar(32) NOT NULL DEFAULT '',
  `os_build` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_user_profile_table: ~5 rows (aproximadamente)
DELETE FROM `mantis_user_profile_table`;
/*!40000 ALTER TABLE `mantis_user_profile_table` DISABLE KEYS */;
INSERT INTO `mantis_user_profile_table` (`id`, `user_id`, `platform`, `os`, `os_build`, `description`) VALUES
	(29, 0, 'Windows_V2', 'OS de 64 bits_V2', '20H2_V2', 'Processador: Intel(R) Core(TM) i5-8265U CPU @ 1.60GHz   1.80 GHz\r\nRAM: 8,00 GB_V2'),
	(31, 0, 'Windows_V2', 'OS de 64 bits_V2', '20H2_V2', 'Processador: Intel(R) Core(TM) i5-8265U CPU @ 1.60GHz   1.80 GHz\r\nRAM: 8,00 GB_V2'),
	(33, 0, 'Windows_V2', 'OS de 64 bits_V2', '20H2_V2', 'Processador: Intel(R) Core(TM) i5-8265U CPU @ 1.60GHz   1.80 GHz\r\nRAM: 8,00 GB_V2'),
	(34, 0, 'Windows_V2', 'OS de 64 bits_V2', '20H2_V2', 'Processador: Intel(R) Core(TM) i5-8265U CPU @ 1.60GHz   1.80 GHz\r\nRAM: 8,00 GB_V2'),
	(36, 0, 'Windows_V2', 'OS de 64 bits_V2', '20H2_V2', 'Processador: Intel(R) Core(TM) i5-8265U CPU @ 1.60GHz   1.80 GHz\r\nRAM: 8,00 GB_V2');
/*!40000 ALTER TABLE `mantis_user_profile_table` ENABLE KEYS */;

-- Copiando estrutura para tabela bugtracker.mantis_user_table
CREATE TABLE IF NOT EXISTS `mantis_user_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) NOT NULL DEFAULT '',
  `realname` varchar(191) NOT NULL DEFAULT '',
  `email` varchar(191) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `enabled` tinyint(4) NOT NULL DEFAULT 1,
  `protected` tinyint(4) NOT NULL DEFAULT 0,
  `access_level` smallint(6) NOT NULL DEFAULT 10,
  `login_count` int(11) NOT NULL DEFAULT 0,
  `lost_password_request_count` smallint(6) NOT NULL DEFAULT 0,
  `failed_login_count` smallint(6) NOT NULL DEFAULT 0,
  `cookie_string` varchar(64) NOT NULL DEFAULT '',
  `last_visit` int(10) unsigned NOT NULL DEFAULT 1,
  `date_created` int(10) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_cookie_string` (`cookie_string`),
  UNIQUE KEY `idx_user_username` (`username`),
  KEY `idx_enable` (`enabled`),
  KEY `idx_access` (`access_level`),
  KEY `idx_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela bugtracker.mantis_user_table: ~2 rows (aproximadamente)
DELETE FROM `mantis_user_table`;
/*!40000 ALTER TABLE `mantis_user_table` DISABLE KEYS */;
INSERT INTO `mantis_user_table` (`id`, `username`, `realname`, `email`, `password`, `enabled`, `protected`, `access_level`, `login_count`, `lost_password_request_count`, `failed_login_count`, `cookie_string`, `last_visit`, `date_created`) VALUES
	(1, 'administrator', '', 'root@localhost', '200ceb26807d6bf99fd6f4f0d1ca54d4', 1, 0, 90, 1123, 0, 0, 'nIuLOVvlErjpfGm4-56A8swTcNMjN2ICY3dOyfr0NlUsYHrj93bH9dOJfohIVuVd', 1647870785, 1645561527),
	(13, 'Lucas.Cunha', 'Lucas Cunha', 'lucas.cunha@base2.com.br', '1810194f177d9a0aa084c320655596ba', 1, 0, 55, 0, 0, 1, 'X0VEXGRGiM0NxWKOANltHgiZSnFJkqBmrPCvvu01BtR3IsLBlAnwOjZ2Vi4b_tey', 1645727741, 1645727741);
/*!40000 ALTER TABLE `mantis_user_table` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
