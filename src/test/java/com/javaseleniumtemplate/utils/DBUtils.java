package com.javaseleniumtemplate.utils;

import java.io.Reader;
import java.sql.*;
import java.util.ArrayList;

import com.ibatis.common.jdbc.ScriptRunner;
import com.javaseleniumtemplate.GlobalParameters;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class DBUtils {

    private static String url = GlobalParameters.DB_URL;
    private static String dbUser = GlobalParameters.DB_USER;
    private static String password = GlobalParameters.DB_PASSWORD;



    public static ArrayList<String> getQueryResult(String query){
        ArrayList<String> arrayList = null;
        Connection connection = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Statement stmt = null;
            connection = DriverManager.getConnection(url, dbUser,password);

            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            if(!rs.isBeforeFirst()){
                return null;
            }

            else{
                int numberOfColumns;
                ResultSetMetaData metadata=null;

                arrayList = new ArrayList<String>();
                metadata = rs.getMetaData();
                numberOfColumns = metadata.getColumnCount();

                while (rs.next()) {
                    int i = 1;
                    while(i <= numberOfColumns) {
                        arrayList.add(rs.getString(i++));
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static void executeQuery(String query){
        Connection connection = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Statement stmt = null;
            connection = DriverManager.getConnection(url, dbUser,password);

            stmt = connection.createStatement();
            stmt.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void updateDatabase(Reader reader){
        Connection connection = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Statement stmt = null;
            connection = DriverManager.getConnection(url, dbUser,password);

            stmt = connection.createStatement();
            ScriptRunner sr = new ScriptRunner(connection, false, true);

            //Running the script
            sr.runScript(reader);

        } catch (Exception e) {
            //e.printStackTrace();
        } finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}
